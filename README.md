# Api da Prefeitura de Extremoz

Backend da aplicação mobile e do Painel de controle.

### Softwares necessários

[Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

[Docker](https://docs.docker.com/install/)

[Docker Compose](https://docs.docker.com/compose/install/)

[Postman (opcional)](https://www.getpostman.com/docs/v6/postman/launching_postman/installation_and_updates)

### Como subir a api

Vá até a pasta raiz do projeto e execute o seguinte arquivo subirContainers.bash

```
sudo /bin/bash subirContainers.bash
```

## Resultado esperado

Dois containers estarão em funcionamento: o da api em Laravel e o do Banco PostgreSQL.

Verifique com o seguinte comando:

```
sudo docker-compose ps
```

O resultado deverá ser parecido com este

```
Name               Command               State           Ports         
-----------------------------------------------------------------------
api     docker-php-entrypoint apac ...   Up      0.0.0.0:80->80/tcp    
apidb   docker-entrypoint.sh postgres    Up      0.0.0.0:5432->5432/tcp

```
