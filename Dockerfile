FROM php:7.1-apache

# Instala PHP e extenxsões
RUN apt-get update && apt-get install -y \
  zlib1g-dev \
  libpq-dev \
  mysql-client \
  libmcrypt-dev \
  libreadline-dev \
  && rm -r /var/lib/apt/lists/* \
  && docker-php-ext-install \
  pdo_pgsql \
  pdo_mysql \
  zip \
  opcache \
  mcrypt


# Instala composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer

# Envia arquivo de configuração do apache para dentro do container
COPY /api/apache2-laravel.conf /etc/apache2/sites-available/laravel.conf

COPY /api /var/www/html

COPY /postInstallWithDB.sh /var/www/html
COPY /postInstallWithoutDB.sh /var/www/html

RUN a2dissite 000-default.conf && a2ensite laravel.conf && a2enmod headers && a2enmod rewrite

RUN usermod -u 1000 www-data && groupmod -g 1000 www-data

WORKDIR /var/www/html
