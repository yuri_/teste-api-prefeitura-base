@component('mail::message')
# Olá {{$usuario->email}}

Obrigado por criar uma conta. Por favor, verifique seu email clicando neste link:

@component('mail::button', ['url' => route('verificar', $usuario->tokenVerificacao)])
Verificar Conta
@endcomponent

Obrigado,<br>
{{ config('app.name') }}
@endcomponent
