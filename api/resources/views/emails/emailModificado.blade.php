@component('mail::message')
# Olá {{$usuario->name}}

Verificamos que você modificou seu email. Por favor, verifique-o novamente clicando neste link:

@component('mail::button', ['url' => route('verificarcontausuario', $usuario->verification_token)])
Verificar Conta
@endcomponent

Obrigado,<br>
{{ config('app.name') }}
@endcomponent
