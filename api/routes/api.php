<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
 * TOKEN
*/
Route::get('tokens', 'Token@buscarTokensSocialEpessoal');
Route::options('/{any}', function(){ return ''; })->where('any', '.*');
Route::name('cadastro')->post('oauth/token', 'Token@issueToken');
/*
 * USUÁRIO
*/
Route::post('usuarios/buscar', 'UsuarioController@buscar');
Route::get('usuarios/perfil', 'UsuarioController@perfil');
Route::put('usuarios/perfil', 'UsuarioController@update');
Route::put('usuarios/perfil/alterarSenha', 'UsuarioController@alterarSenha');
Route::put('usuarios/perfil/alterarImagem', 'UsuarioController@alterarImagem');
Route::get('usuarios/{user}', 'UsuarioController@show');
Route::resource('usuarios', 'UsuarioController');
Route::name('verificar')->get('usuarios/verificar/{token}', 'UsuarioController@verificar');

/*
 * PROTOCOLO
*/
Route::get('protocolos/meusProtocolos', 'protocolos\ProtocoloController@meusProtocolos');
Route::resource('protocolos', 'protocolos\ProtocoloController');
Route::resource('agrupamento/protocolo', 'protocolos\AgrupamentoProtocoloController');
Route::post('protocolos/anonimo', 'protocolos\ProtocoloController@cadastrarProtocoloSemUsuario');
Route::get('protocolos/{protocolo}', 'protocolos\ProtocoloController@show');
Route::get('protocolos/{id}/mensagens', 'protocolos\ProtocoloController@mensagensPorProtocolo');
Route::post('protocolos/{id}/mensagens', 'protocolos\ProtocoloController@mensagensPorProtocoloCriar');

// /*
//  * SERVIÇO
// */
//
// Route::post('servicos', 'protocolos\servicos\ServicoController@store');
// Route::post('servicos/anonimo', 'protocolos\servicos\ServicoController@cadastrarSemUsuario');
  Route::resource('subCategoriaServicos', 'protocolos\SubCategoriaServicoController');
  Route::resource('categoriaServicos', 'protocolos\servicos\CategoriaServicoController');
// Route::resource('servicos', 'protocolos\servicos\ServicoController');

// /*
//  * OUVIDORIA
// */
//
// Route::post('ouvidorias', 'ouvidoria\OuvidoriaController@store');
// Route::post('ouvidorias/anonimo', 'ouvidoria\OuvidoriaController@cadastrarSemUsuario');

/*
 * ENQUETE
*/

Route::post('enquetes', 'enquete\EnqueteController@store');
Route::put('enquetes/{id}', 'enquete\EnqueteController@update');
Route::get('enquetes', 'enquete\EnqueteController@index');
Route::get('enquetes/{id}', 'enquete\EnqueteController@show');
Route::post('enquetes/{id}/votar', 'enquete\EnqueteController@votar');
/*
 * NOTÍCIAS
*/
Route::resource('noticias', 'noticias\NoticiaController');
/*
 * CONFIGURAÇÕES
*/
Route::get('configs', 'configuracoes\ConfiguracoesController@index');
Route::get('configs/{nome}', 'configuracoes\ConfiguracoesController@show');
Route::post('configs', 'configuracoes\ConfiguracoesController@store');
Route::put('configs/{nome}', 'configuracoes\ConfiguracoesController@update');
Route::resource('templates', 'configuracoes\TemplateController');

/*
 * NOTIFICACOES
*/
Route::get('notificacoes', 'notificacoes\NotificacaoController@index');
Route::get('notificacoes/{id}', 'notificacoes\NotificacaoController@show');
Route::post('notificacoes', 'notificacoes\NotificacaoController@store');
Route::put('notificacoes/{id}', 'notificacoes\NotificacaoController@update');


  Route::resource('categoriaNoticias', 'noticias\CategoriaNoticiaController');

  Route::resource('categoriaInformacoes', 'informacoes\CategoriaInformacaoController');
  Route::resource('informacoes', 'informacoes\InformacaoController');

  Route::resource('status', 'protocolos\StatusController');
  Route::resource('protocoloRespostas', 'protocolos\ProtocoloRespostaController');

  Route::resource('enquetes', 'enquete\EnqueteController');

  Route::get('login/google', 'Auth\LoginController@redirectToProvider');
  Route::get('login/google/callback', 'Auth\LoginController@handleProviderCallback');

  Route::get('login/facebook', 'Auth\LoginController@redirectToProvider');
  Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback');
