<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class ProtocoloResposta extends Model
{
  protected $fillable = [
      'autor',
      'interno',
      'mensagem',
      'protocoloId',
  ];

  protected $hidden = [
      'updated_at',
  ];

  /**
   * RELACIONAMENTOS
   */

  public function usuario()
  {
      return $this->belongsTo('App\User', 'autor');
  }

public function getCreatedAtAttribute($value)
{
  return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d/m/Y - H:i');
}

}
