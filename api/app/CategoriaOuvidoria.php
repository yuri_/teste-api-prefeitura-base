<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;


class CategoriaOuvidoria extends Model
{
  protected $fillable = [
    'titulo',
  ];

  protected $hidden = [
      'updated_at',
  ];

  /**
   * RELACIONAMENTOS
   */

  public function ouvidorias()
  {
      return $this->hasMany('App\Ouvidoria', 'categoria');
  }

  public function getCreatedAtAttribute($value)
  {
    return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d/m/Y H:i');
  }
}
