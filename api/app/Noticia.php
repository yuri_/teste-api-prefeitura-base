<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Noticia extends Model
{
  protected $fillable = [
    'titulo',
    'imagem',
    'categoria',
    'conteudo',
  ];

  protected $hidden = [
      'updated_at',
  ];


  public function categoria()
  {
      return $this->belongsTo('App\CategoriaNoticia', 'categoria');
  }
  /**
   * MUTADORES
   */

  public function getCreatedAtAttribute($value)
  {
    return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d/m/Y - H:i');
  }
}
