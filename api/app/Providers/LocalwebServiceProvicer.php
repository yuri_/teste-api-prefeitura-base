<?php

namespace App\Providers;

use Aws\S3\S3Client;
use League\Flysystem\AwsS3v2\AwsS3Adapter;
use League\Flysystem\Filesystem;
use Storage;
use Illuminate\Support\ServiceProvider;

class LocalwebServiceProvicer extends ServiceProvider
{
    const LOCALWEB_BASE_URL = 'https://lss.locaweb.com.br';

    /**
     * Bootstrap the application services.
     *
     * @return void
     */ 
    public function boot()
    {
        Storage::extend('localweb', function ($app, $config) {
            $config['base_url'] = isset($config['base_url']) ? $config['base_url'] : self::LOCALWEB_BASE_URL;

            $client = S3Client::factory($config);

            return new Filesystem(new AwsS3Adapter($client, $config['bucket']));
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
