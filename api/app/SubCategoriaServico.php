<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class SubCategoriaServico extends Model
{
  protected $fillable = [
      'titulo',
      'categoriaServico',
  ];

  protected $hidden = [
      'updated_at',
  ];

  /**
   * MUTADORES
   */

  public function getCreatedAtAttribute($value)
  {
    return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d/m/Y H:i');
  }
}
