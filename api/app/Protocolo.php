<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Protocolo extends Model
{
  protected $fillable = [
      'autor',
      'nome',
      'imagem',
      'latitude',
      'longitude',
      'mensagem',
      'categoriaServico',
      'subCategoriaServico',
  ];

  protected $hidden = [
      'updated_at', 'idAuxiliar',
  ];

  /**
   * RELACIONAMENTOS
   */

  public function status()
  {
      return $this->belongsTo('App\Status', 'status');
  }

  public function usuario()
  {
      return $this->belongsTo('App\User', 'autor');
  }

  public function categoriaServico()
  {
      return $this->belongsTo('App\CategoriaServico', 'categoriaServico');
  }

  public function subCategoriaServico()
  {
      return $this->belongsTo('App\SubCategoriaServico', 'subCategoriaServico');
  }

  public function respostas()
  {
      return $this->hasMany('App\ProtocoloResposta', 'protocoloId');
  }

  /**
   * MUTADORES
   */

  public function getCreatedAtAttribute($value)
  {
    return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d/m/Y');
  }
}
