import { NgFlashMessageService } from 'ng-flash-messages';
import { ActivatedRoute, Router } from '@angular/router';
import { APIService } from './../../api.service';
import { Component, OnInit, ViewChild } from "@angular/core";
import { Validators, FormGroup, FormControl } from "@angular/forms";
import { DynamicFormComponent } from "./../../components/dynamic-form/dynamic-form.component";

@Component({
  selector: 'app-noticias-update',
  templateUrl: './noticias-update.component.html',
  styleUrls: ['./noticias-update.component.scss']
})
export class NoticiasUpdateComponent implements OnInit {

  @ViewChild(DynamicFormComponent) form: DynamicFormComponent;
  public selectedFile;
  public regConfig: any[];
  public imagemInput;
  public loading: boolean = false;
  public fileGroup: any;
  public opcoesCategoria: Array<any> = [];
  public dataNoticia: any;
  constructor(private apiService: APIService, private route: ActivatedRoute,
    private ngFlashMessageService: NgFlashMessageService, private router: Router) {
    this.fileGroup = new FormGroup({
      firstName: new FormControl()
    });
  }

  ngOnInit() {
    this.loading = true;
    let id = this.route.snapshot.params['id'];
    this.apiService.getNoticia(id).subscribe(
      data => {
        this.dataNoticia = data;
        this.getCategoriasNoticia().then(
          () => {
            this.loading = false;
            this.criarForm();
          },
          () => this.lancarErro()
        )
      },
      () => {
        this.lancarErro();
      }
    )
  }

  public lancarMensagem(mensagem, tipo) {
    this.ngFlashMessageService.showFlashMessage({
      messages: [mensagem],
      dismissible: true,
      timeout: 3000,
      type: tipo
    });
  }

  getCategoriasNoticia() {
    return this.apiService.getCategoriasNoticia().toPromise().then((response) => {
      let resposta: any = response;
      for (let index = 0; index < resposta.length; index++) {
        const element = response[index];
        this.opcoesCategoria.push({ name: element['nome'], value: element['id'] })
      }
    });
  };

  get noticiaJson() {
    return JSON.stringify(this.form.value);
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0]
  }

  updateNoticia($event) {
    this.loading = true;
    const formdata: FormData = new FormData();
    if (this.selectedFile) {
      formdata.append("imagem", this.selectedFile, "arquivo.jpg");
    }
    formdata.append("categoria", this.form.value.categoria);
    formdata.append("titulo", this.form.value.titulo);
    formdata.append("conteudo", this.form.value.conteudo);
    formdata.append("_method", 'put');
    this.apiService.updateNoticia(this.dataNoticia.id, formdata).subscribe(
      (response) => {
        this.lancarMensagem("Noticia salva com sucesso!", "success");
        this.loading = false;
        this.router.navigate(['/noticias']);
      },
      () => this.lancarErro()
    );
  };

  lancarErro() {
    this.lancarMensagem("Erro ao atualizar a noticia, tente novamente mais tarde!", "danger");
    this.router.navigate(['/noticias']);
  }
  criarForm() {
    this.regConfig = [
      {
        type: "input",
        label: "Titulo",
        inputType: "text",
        name: "titulo",
        value: this.dataNoticia['titulo'],
        validations: [
          {
            name: "required",
            validator: Validators.required,
            message: "Titulo é requerido"
          },
        ]
      },
      {
        type: "select",
        label: "Categoria",
        name: "categoria",
        value: this.dataNoticia['categoria']['id'],
        options: this.opcoesCategoria,
        validations: [
          {
            name: "required",
            validator: Validators.required,
            message: "Categoria é requerido"
          },
        ]
      },
      {
        type: "html",
        label: "Conteudo",
        name: "conteudo",
        value: this.dataNoticia['conteudo'],
      },
      {
        type: "button",
        label: "Salvar"
      }
    ];
  }
}
