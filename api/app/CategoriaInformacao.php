<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class CategoriaInformacao extends Model
{
  protected $fillable = [
    'titulo',
  ];

  protected $hidden = [
      'updated_at',
  ];

  /**
   * RELACIONAMENTOS
   */

  public function informacoes()
  {
      return $this->hasMany('App\Informacao', 'categoria');
  }

  /**
   * MUTADORES
   */

  public function getCreatedAtAttribute($value)
  {
    return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d/m/Y H:i');
  }
}
