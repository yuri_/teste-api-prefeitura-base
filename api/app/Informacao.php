<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Informacao extends Model
{
  protected $fillable = [
    'titulo',
    'descricao',
    'imagem',
    'conteudo',
    'categoria',
  ];

  protected $hidden = [
      'updated_at',
  ];

  /**
   * RELACIONAMENTOS
   */

  public function categoria()
  {
      return $this->belongsTo('App\CategoriaInformacao', 'categoria');
  }

  /**
   * MUTADORES
   */

  public function getCreatedAtAttribute($value)
  {
    return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d/m/Y - H:i');
  }
}
