<?php

namespace App\Http;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\LengthAwarePaginator;

trait ApiResponser
{
	private function sucessoResponse($colecao, $status)
	{
		return response()->json(['data' => $colecao, 'code' => $status], $status);
	}

	protected function erroResponse($mensagem, $status)
	{
		return response()->json(['error' => $mensagem, 'code' => $status], $status);
	}

	protected function mostrarTodos(Collection $colecao, $status = 200)
	{
		if ($colecao->isEmpty()) {
			return $this->sucessoResponse($colecao, $status);
		}

		//$transformer = $colecao->first()->transformer;

		//$colecao = $this->filterData($colecao, $transformer);
		//$colecao = $this->sortData($colecao, $transformer);
		$colecao = $this->paginar($colecao);
		//$colecao = $this->transformData($colecao, $transformer);
		//$colecao = $this->cacheResponse($colecao);

		return $this->sucessoResponse($colecao, $status);
	}

	protected function mostrarUm(Model $objeto, $status = 200)
	{
		//$transformer = $instance->transformer;

		//$instance = $this->transformData($instance, $transformer);

		return response()->json($objeto, $status);

		//	return $this->sucessoResponse($objeto, $status);
	}

	protected function mostrarMensagem($mensagem, $status = 200)
	{
		return $this->sucessoResponse($mensagem, $status);
	}

	protected function filterData(Collection $colecao, $transformer)
	{
		foreach (request()->query() as $query => $value) {
			$attribute = $transformer::originalAttribute($query);

			if (isset($attribute, $value)) {
				$colecao = $colecao->where($attribute, $value);
			}
		}

		return $colecao;
	}

	protected function sortData(Collection $colecao, $transformer)
	{
		if (request()->has('sort_by')) {
			$attribute = $transformer::originalAttribute(request()->sort_by);

			$colecao = $colecao->sortBy->{$attribute};
		}

		return $colecao;
	}

	protected function paginar(Collection $colecao)
	{
		$rules = [
			'porPagina' => 'integer|min:2|max:50',
		];

		Validator::validate(request()->all(), $rules);

		$pagina = LengthAwarePaginator::resolveCurrentPage();

		$porPagina = 15;
		if (request()->has('porPagina')) {
			$porPagina = (int) request()->porPagina;
		}

		$results = $colecao->slice(($pagina - 1) * $porPagina, $porPagina)->values();

		$paginado = new LengthAwarePaginator($results, $colecao->count(), $porPagina, $pagina, [
			'path' => LengthAwarePaginator::resolveCurrentPath(),
		]);

		$paginado->appends(request()->all());

		return $paginado;

	}

	protected function transformData($data, $transformer)
	{
		$transformation = fractal($data, new $transformer);

		return $transformation->toArray();
	}

	protected function cacheResponse($data)
	{
		$url = request()->url();
		$queryParams = request()->query();

		ksort($queryParams);

		$queryString = http_build_query($queryParams);

		$fullUrl = "{$url}?{$queryString}";

		return Cache::remember($fullUrl, 30/60, function() use($data) {
			return $data;
		});
	}
}
