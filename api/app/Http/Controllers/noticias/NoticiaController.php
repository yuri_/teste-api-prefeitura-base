<?php

namespace App\Http\Controllers\noticias;

use App\Noticia;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\midias\ImagemController;
use Illuminate\Support\Facades\Storage;

class NoticiaController extends ApiController
{
    protected $imagemController;
    protected $nomeModelo;

    public function __construct(ImagemController $iC)
    {
      $this->imagemController = $iC;
      $this->nomeModelo = 'noticias';
      $this->middleware('auth:api')->except('index', 'show', 'testeImagem');
    }

    /**
     * Retorna lista de Noticia.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->mostrarTodos(Noticia::all()->load(['categoria'])->reverse());

    }

    /**
     * Cria uma notícia.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($request->user()->isAdmin()){
        $this->validarModelo($request);

        return DB::transaction(function() use($request) {

          $urlImagem = $this->imagemController->salvarUmaImagem($request);

          $novoArray = array_merge($request->all(), ['imagem' => $urlImagem]);

          return $this->mostrarUm(Noticia::create($novoArray));
        });
      }else{
        return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);
      }
    }

    private function validarModelo($request)
    {
      $request->validate([
        'categoria' => 'required|exists:categoria_noticias,id',
        'titulo' => 'required|min:2|max:150',
        'conteudo' => 'required|min:10|max:200000',
        'imagem' => 'required|image',
      ]);
    }

    /**
     * Retorna Noticia por id.
     *
     * @param  \App\Noticia  $noticia
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->mostrarUm(Noticia::findOrFail($id)->load(['categoria']));
    }

    /**
     * Atualiza Noticia por id.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Noticia  $noticia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if($request->user()->isAdmin()){

        $modelo = Noticia::findOrFail($id);

        $request->validate([
          'categoria' => 'exists:categoria_noticias,id',
          'titulo' => 'min:2|max:150',
          'conteudo' => 'min:10|max:200000',
          'imagem' => 'image',
        ]);

        return DB::transaction(function() use($request, $modelo, $id) {

          if($request->imagem == true){

            $urlImagem = $this->imagemController->salvarUmaImagem($request);

            $this->imagemController->removerUmaImagem($modelo->imagem);

            $novoArray = array_merge($request->all(), ['imagem' => $urlImagem]);

            $modelo->update($novoArray);

            return $this->mostrarUm(Noticia::findOrFail($id));
          }
          $modelo->update($request->all());
          return $this->mostrarUm(Noticia::findOrFail($id));
        });
      }else{
        return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);
      }
    }

    /**
     * Remove Noticia por id.
     *
     * @param  \App\Noticia  $noticia
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
      if($request->user()->isAdmin()){

        return DB::transaction(function() use($request, $id) {

          $modelo = \App\Noticia::findOrFail($id);

          $modelo->delete();

          $this->imagemController->removerUmaImagem($modelo->imagem);

          return $this->mostrarUm($modelo);

        });

      }
      return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);
    }
}
