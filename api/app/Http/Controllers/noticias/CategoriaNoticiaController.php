<?php

namespace App\Http\Controllers\noticias;

use App\CategoriaNoticia;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class CategoriaNoticiaController extends ApiController
{

  public function __construct()
  {
    $this->middleware('auth:api');
  }
    /**
     * Retorna lista de Noticia.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      if($request->user()->isAdmin()){
        return CategoriaNoticia::all();
      }
      return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);
    }

    /**
     * Cria uma notícia.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
       if($request->user()->isAdmin()){
         $this->validarModelo($request);

         return $this->mostrarUm(CategoriaNoticia::create($request->all()));
       }
       return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);

     }

     private function validarModelo($request)
     {
       $request->validate([
         'nome' => 'required|min:2|max:25|unique:categoria_noticias',
       ]);
     }

    /**
     * Retorna CategoriaNoticia por id.
     *
     * @param  \App\CategoriaNoticia  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
      if($request->user()->isAdmin()){
        return $this->mostrarUm(CategoriaNoticia::findOrFail($id));
      }
      return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);

    }

    /**
     * Atualiza CategoriaNoticia por id.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CategoriaNoticia  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if($request->user()->isAdmin()){

        $request->validate([
          'nome' => 'required|min:2|max:25|unique:categoria_noticias',
        ]);

        $modelo = CategoriaNoticia::findOrFail($id);

        $modelo->update($request->all());

        return $this->mostrarUm(CategoriaNoticia::findOrFail($id));
      }
      return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);

    }

    /**
     * Remove CategoriaNoticia por id.
     *
     * @param  \App\CategoriaNoticia  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
      if($request->user()->isAdmin()){

        $modelo = \App\CategoriaNoticia::findOrFail($id);

        $modelo->delete();

        return $this->mostrarUm($modelo);
      }
      return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);

    }
}
