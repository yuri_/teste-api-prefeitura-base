<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;

use Socialite;

class LoginController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->stateless()->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        //$user = Socialite::driver('google')->user();

         $user = Socialite::driver('facebook')->stateless()->user();

         return $user->getName();

        // $user->token;
    }
}
