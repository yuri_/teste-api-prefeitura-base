<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\DB;
use \Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\midias\ImagemController;

class UsuarioController extends ApiController
{
  protected $imagemController;

  public function __construct(ImagemController $iC)
  {
      $this->imagemController = $iC;
      $this->middleware('auth:api')->except('store', 'verificar');
      $this->middleware('can:view,user')->only('show');
  }

    /**
     * Retorna lista de User.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return ($request->user()->isAdmin()) ? $this->mostrarTodos(User::all()) : response()->json(['error' => 'Não autorizado', 'code' => 401], 401);
    }

    /**
     * Cria uma notícia.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validarModelo($request);

      $request->request->add(['password' => bcrypt($request->password)]);
      $request->request->add(['verificado' => User::USU_NAO_VERIFICADO]);
      $request->request->add(['tokenVerificacao' => str_random(40)]);
      $request->request->add(['admin' => User::REGULAR]);

      return DB::transaction(function() use($request) {

        if($request->has('imagem')){
          $urlImagem = $this->imagemController->salvarUmaImagem($request);

          $novoArray = array_merge($request->all(), ['imagem' => $urlImagem]);

          $usu = User::create($novoArray);
          return response()->json(['data' => $usu,
              'mensagem'=> 'Cadastrado com sucesso! Por favor confirme sua conta no email cadastrado.',
              'code' => 200], 200);
        }
        $usu = User::create($request->all());

        return response()->json(['data' => $usu,
            'mensagem'=> 'Cadastrado com sucesso! Por favor confirme sua conta no email cadastrado.',
            'code' => 200], 200);
      });
    }


    private function validarModelo($request)
    {
      $request->validate([
        'name' => 'required|min:2|max:120',
        'email' => 'required|email|unique:users',
        'password' => 'required|min:2|max:80',
        'fone' => 'digits_between:9,15|unique:users,fone',
        'imagem' => 'image',
      ]);
    }

    /**
     * Retorna User por id.
     *
     * @param  \App\User  $User
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
      return $this->mostrarUm($user);
    }

    /**
     * Atualiza User por id.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $User
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $request->validate([
        'name' => 'min:2|max:120',
        'fone' => 'digits_between:9,15',
      ]);

      if($request->has('email')) $request->request->remove('email');
      if($request->has('verificado')) $request->request->remove('verificado');
      if($request->has('tokenVerificacao')) $request->request->remove('tokenVerificacao');
      if($request->has('password')) $request->request->remove('password');
      if($request->has('imagem')) $request->request->remove('imagem');

      User::where('id', $request->user()->id)->update($request->all());

      $modelo = User::findOrFail($request->user()->id);
      $modelo->makeHidden(['email_verified_at', 'verificado', 'admin']);

      return $this->mostrarUm($modelo);
    }

    /**
     * Remove User por id.
     *
     * @param  \App\User  $User
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $modelo = \App\User::findOrFail($id);

      $modelo->delete();

      return $this->mostrarUm($modelo);
    }

    public function verificar($token)
    {
        $user = User::where('tokenVerificacao', $token)->firstOrFail();

        $user->verificado = User::USU_VERIFIDADO;
        $user->tokenVerificacao = null;
        $user->email_verified_at = Carbon::now();

        $user->save();

        return $this->mostrarMensagem('Sua conta foi verificada com sucesso!');
    }

    public function perfil(Request $request)
    {
      return $request->user();
    }

    public function alterarSenha(Request $request)
    {
      $request->validate([
        'password' => 'required|min:2|max:80',
        'newPassword' => 'required|min:2|max:80',
      ]);

      $antigaSenha =  User::where('id', $request->user()->id)->first()->password;

      if (password_verify($request->password, $antigaSenha)) {

        $request->request->add(['newPassword' => bcrypt($request->newPassword)]);

        User::where('id', $request->user()->id)->update(['password' => $request->newPassword]);

        return response()->json(['Mensagem' => 'Senha alterada com sucesso!', 'code' => 200], 200);

      } else {
          return response()->json(['error' => 'Senha antiga não é válida', 'code' => 401], 401);
      }


    }

    public function alterarImagem(Request $request)
    {
      $request->validate([
        'imagem' => 'required|image',
      ]);

      $modelo = $request->user();

      return DB::transaction(function() use($request, $modelo) {

        if($modelo->imagem != null) $this->imagemController->removerUmaImagem($modelo->imagem);

        $urlImagem = $this->imagemController->salvarUmaImagem($request);

        User::where('id', $modelo->id)->first()->update(['imagem' => $urlImagem]);

        return $this->mostrarUm($request->user());

      });

    }

    public function buscar(Request $request)
    {
      if($request->user()->isAdmin()){

        $request->validate([
          'id' => 'required_without_all:nome,email',
          'nome' => 'required_without_all:id,email',
          'email' => 'required_without_all:id,nome',
        ]);

        if($request->has('id')){
          return User::findOrFail($request->id);
        }

        if($request->has('nome')){
          return $users = DB::table('users')
                  ->where('name', 'like', $request->nome . '%')
                  ->get();
        }

        if($request->has('email')){
          return $users = DB::table('users')
                  ->where('email', 'like', $request->email . '%')
                  ->get();
        }
      }
      return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);
    }
}
