<?php

namespace App\Http\Controllers;

use Psr\Http\Message\ServerRequestInterface;
use Laravel\Passport\Http\Controllers\AccessTokenController;
use Zend\Diactoros\Response as Psr7Response;
use Illuminate\Support\Facades\DB;

class Token extends AccessTokenController{

  public function issueToken(ServerRequestInterface $request)
  {
      if(array_key_exists("username", $request->getParsedBody())){
        $usu = \App\User::where('email', $request->getParsedBody()['username'])->firstOrFail();

        if(!$usu->verificado){
          return response()->json(['error' => 'Não autorizado. Por favor confirme o cadastro no seu email.', 'code' => 401], 401);
        }
      }
      return $this->withErrorHandling(function () use ($request) {
          return $this->convertResponse(
              $this->server->respondToAccessTokenRequest($request, new Psr7Response)
          );
      });
  }

  public function buscarTokensSocialEpessoal()
  {
    $token = [];
    $token['social'] = DB::table('oauth_clients')->where('id', 1)->first();
    $token['pessoal'] = DB::table('oauth_clients')->where('id', 2)->first();
    $resposta = [
      'pessoal' => [
        'id'=> $token['pessoal']->id,
        'token'=> $token['pessoal']->secret,
      ],
      'social' => [
        'id'=> $token['social']->id,
        'token'=> $token['social']->secret,
      ]
    ];
    return $resposta;

  }
}
