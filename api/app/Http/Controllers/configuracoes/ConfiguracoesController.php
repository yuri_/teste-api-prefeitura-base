<?php

namespace App\Http\Controllers\configuracoes;

use App\Configuracoes;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class ConfiguracoesController extends ApiController
{
  public function __construct()
  {
    $this->middleware('auth:api');
  }
    /**
     * Retorna lista de Configuracoes.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return ($request->user()->isAdmin()) ? $this->mostrarTodos(Configuracoes::all()) : response()->json(['error' => 'Não autorizado', 'code' => 401], 401);
    }

    /**
     * Cria uma Configuracão.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($request->user()->isAdmin()){

        $this->validarModelo($request);

        return $this->mostrarUm(Configuracoes::create($request->all()));
      }
      return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);
    }

    private function validarModelo($request)
    {
      $request->validate([
        'campo' => 'required|min:2|max:128|unique:configuracoes,campo',
        'valor' => 'required|min:1|max:400',
      ]);
    }

    public function update(Request $request, $nomeCampo)
    {
      if($request->user()->isAdmin()){

        if ($request->has('campo')) $request->request->remove('campo');

        $modelo =  Configuracoes::where('campo', $nomeCampo)->firstOrfail();
        $modelo->fill($request->all());
        $modelo->save();

        return $modelo;

      }
      return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);
    }

    /**
     * Retorna Configuracoes por id.
     *
     * @param  \App\Configuracoes  $Configuracoes
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $nomeCampo)
    {
      if($request->user()->isAdmin()){

        $modelo =  Configuracoes::where('campo', $nomeCampo)->first();

        return ($modelo) ? $modelo : response()->json(['error' => 'Não existe este campo.', 'code' => 404], 404);;

      }
      return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);
    }
}
