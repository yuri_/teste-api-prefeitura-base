<?php

namespace App\Http\Controllers\configuracoes;

use App\Template;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class TemplateController extends ApiController
{

  public function __construct()
  {
    $this->middleware('auth:api');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        return ($request->user()->isAdmin()) ? Template::all() : response()->json(['error' => 'Não autorizado', 'code' => 401], 401);
    }

    public function show(Request $request, $id)
    {
      return ($request->user()->isAdmin()) ? Template::findOrFail($id) : response()->json(['error' => 'Não autorizado', 'code' => 401], 401);
    }


    public function store(Request $request)
    {
      if($request->user()->isAdmin()){

        $this->validarModelo($request);

        return $this->mostrarUm(Template::create($request->all()));
      }
      return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);
    }

    private function validarModelo($request)
    {
      $request->validate([
        'nome' => 'required|min:1|max:50|unique:templates,nome',
        'cor' => 'min:1|max:50',
      ]);
    }

    public function update(Request $request, $id)
    {
      if($request->user()->isAdmin()){

        $request->validate([
          'nome' => 'min:1|max:50',
          'cor' => 'min:1|max:50',
        ]);

        $modelo =  Template::findOrFail($id);

        $modelo->update($request->all());

        return $modelo;

      }
      return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);
    }

    public function destroy($id)
    {
      $modelo = \App\Template::findOrFail($id);

      $modelo->delete();

      return $modelo;
    }

}
