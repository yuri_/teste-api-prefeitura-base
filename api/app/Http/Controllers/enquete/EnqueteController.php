<?php

namespace App\Http\Controllers\enquete;
use App\Enquete;
use App\VotoEnquete;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\enquete\VotoOpcaoController;
use App\Http\Controllers\enquete\VotoEnqueteController;

class EnqueteController extends ApiController
{
  public function __construct()
  {
      $this->middleware('auth:api')->only('votar');
      $this->middleware('auth:api')->only('update');
  }
    /**
     * Retorna lista de Enquete.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $colecao = Enquete::all();

        foreach ($colecao as $key => $value) {
          $value->makeHidden('resPrivado');
          $value->encerramento = Carbon::createFromFormat('d/m/Y - H:i', $value->created_at)->addHours($value->duracaoHoras)->format('d/m/Y - H:i');
          $value->encerrado = ($value->encerramento > Carbon::now()) ? true : false;
        }
        return $this->mostrarTodos($colecao);
    }

    /**
     * Cria uma notícia.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validarModelo($request);

      $voc = new VotoOpcaoController();

      $retorno = DB::transaction(function() use($request, $voc) {

        $modeloSalvo = Enquete::create($request->all());

        $voc->store($request, $modeloSalvo->id);

        return $modeloSalvo;
      });

      return $this->mostrarUm($retorno->load(['opcoes']));

    }

    private function validarModelo($request)
    {
      $request->validate([
        'opcoes' => 'required|array',
        'opcoes.*.titulo' => 'required|min:2|max:20',
        'opcoes.*.descricao' => 'required|min:2|max:128',
        'duracaoHoras' => 'required|integer',
        'titulo' => 'required|min:2|max:80',
        'descricao' => 'required|min:10|max:4000',
        'resPrivado' => 'required|boolean',
      ]);
    }

    /**
     * Retorna Enquete por id.
     *
     * @param  \App\Enquete  $Enquete
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $modelo = Enquete::findOrFail($id);

        $modelo->load(['opcoes']);
        $modelo['encerramento'] = Carbon::createFromFormat('d/m/Y - H:i', $modelo->created_at)->addHours($modelo->duracaoHoras)->format('d/m/Y - H:i');
        $modelo['encerrado'] = ($modelo->encerramento > Carbon::now()) ? true : false;
        //$modelo->makeHidden('resPrivado');
        $usuario = auth('api')->user();
        $voto = null;
        if($usuario) $voto = VotoEnquete::where('autor', $usuario->id)->where('enquete', $id)->first();
        if($voto) $modelo['voto'] = $voto->votoOpcao;
        else $modelo['voto'] = null;
        if(! $modelo->resPrivado){
          foreach ($modelo->opcoes as $key => $value) {
            $value['votos'] = DB::table('resultado_enquetes')->where('votoOpcao', $value->id)->first()->votos;
          }
        }

        return $this->mostrarUm($modelo);
    }

    /**
     * Atualiza Enquete por id.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Enquete  $Enquete
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if($request->user()->isAdmin()){

        $request->validate([
          'duracaoHoras' => 'integer',
          'titulo' => 'min:2|max:80',
          'descricao' => 'min:10|max:4000',
          'resPrivado' => 'boolean',
        ]);

        $modelo = Enquete::findOrFail($id);

        $modelo->update($request->all());

        return $this->mostrarUm(Enquete::findOrFail($id));
      }
    }

    /**
     * Remove Enquete por id.
     *
     * @param  \App\Enquete  $Enquete
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $modelo = \App\Enquete::findOrFail($id);

      $modelo->delete();

      return $modelo;
    }

    public function votar(Request $request)
    {
      $request->request->add(['autor' => $request->user()->id]);

      $retorno = DB::transaction(function() use($request) {

        $vte = new VotoEnqueteController();

        $modeloSalvo = $vte->store($request);

        DB::table('resultado_enquetes')->where('votoOpcao', $modeloSalvo->votoOpcao)->increment('votos', 1);
        return $modeloSalvo;
      });
      return $retorno;

    }
}
