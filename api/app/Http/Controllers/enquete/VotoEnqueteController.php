<?php

namespace App\Http\Controllers\enquete;

use App\VotoEnquete;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\DB;

class VotoEnqueteController extends ApiController
{

    /**
     * Retorna lista de VotoEnquete.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->mostrarTodos(VotoEnquete::all());
    }

    /**
     * Cria uma notícia.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
        'enquete' => 'required|exists:enquetes,id',
        'votoOpcao' => 'required|exists:voto_opcaos,id',
        'autor' => 'required|exists:users,id|unique:voto_enquetes,autor,NULL,id,enquete,'.$request->enquete,
      ]);

      return VotoEnquete::create($request->all());
    }



    /**
     * Retorna VotoEnquete por id.
     *
     * @param  \App\VotoEnquete  $VotoEnquete
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->mostrarUm(VotoEnquete::findOrFail($id));
    }


    /**
     * Remove VotoEnquete por id.
     *
     * @param  \App\VotoEnquete  $VotoEnquete
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $modelo = \App\VotoEnquete::findOrFail($id);

      $modelo->delete();

      return $this->mostrarUm($modelo);
    }
}
