<?php

namespace App\Http\Controllers\enquete;

use App\ResultadoEnquete;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\DB;

class ResultadoEnqueteController extends ApiController
{

    /**
     * Retorna lista de ResultadoEnquete.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->mostrarTodos(ResultadoEnquete::all());
    }

    /**
     * Cria uma notícia.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
        'enquete' => 'required|exists:enquetes,id',
        'votoOpcao' => 'required|exists:voto_opcaos,id',
      ]);

      return ResultadoEnquete::create($request->all());
    }



    /**
     * Retorna ResultadoEnquete por id.
     *
     * @param  \App\ResultadoEnquete  $ResultadoEnquete
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->mostrarUm(ResultadoEnquete::findOrFail($id));
    }


    /**
     * Remove ResultadoEnquete por id.
     *
     * @param  \App\ResultadoEnquete  $ResultadoEnquete
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $modelo = \App\ResultadoEnquete::findOrFail($id);

      $modelo->delete();

      return $this->mostrarUm($modelo);
    }
}
