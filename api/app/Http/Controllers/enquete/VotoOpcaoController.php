<?php

namespace App\Http\Controllers\enquete;

use App\VotoOpcao;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\DB;

class VotoOpcaoController extends ApiController
{

    /**
     * Retorna lista de VotoOpcao.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->mostrarTodos(VotoOpcao::all());
    }

    /**
     * Cria uma notícia.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $enqueteId)
    {
      foreach ($request->opcoes as $key => $value) {

        $value['enquete'] = $enqueteId;

        $retorno = DB::transaction(function() use($request, $value, $enqueteId) {

          $modeloSalvo = VotoOpcao::create($value);

          $value['votoOpcao'] = $modeloSalvo->id;
          $value['votos'] = 0;

          return \App\ResultadoEnquete::create($value);
        });

      }
      return $retorno;
    }



    /**
     * Retorna VotoOpcao por id.
     *
     * @param  \App\VotoOpcao  $VotoOpcao
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->mostrarUm(VotoOpcao::findOrFail($id));
    }


    /**
     * Remove VotoOpcao por id.
     *
     * @param  \App\VotoOpcao  $VotoOpcao
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $modelo = \App\VotoOpcao::findOrFail($id);

      $modelo->delete();

      return $this->mostrarUm($modelo);
    }
}
