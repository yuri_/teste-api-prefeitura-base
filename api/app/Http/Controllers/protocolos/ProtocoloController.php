<?php

namespace App\Http\Controllers\protocolos;

use App\Protocolo;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\DB;
use App\ProtocoloResposta;
use App\Http\Controllers\midias\ImagemController;

class ProtocoloController extends ApiController
{
  protected $imagemController;

  public function __construct(ImagemController $iC)
  {
      $this->middleware('auth:api')->except('show', 'cadastrarProtocoloSemUsuario', 'mensagensPorProtocolo');
      $this->imagemController = $iC;
  }

    /**
     * Retorna lista de Protocolo.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->mostrarTodos(Protocolo::all()->load(['status', 'categoriaServico', 'subCategoriaServico']));
    }

    /**
     * Cria um protocolo.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
         if($request->user()){
            $request->request->add(['autor' => $request->user()->id]);
            $request->request->remove('nome');
         }

         $this->validarModelo($request);

         $req = $request->all();

         $req = array_merge($req, ['categoriaServico' => $request->categoriaServico]);
         $req = array_merge($req, ['subCategoriaServico' => $request->subCategoriaServico]);
         $req = array_merge($req, ['status' => 1]);

         return DB::transaction(function() use($request, $req) {

           if($request->has('imagem')){

             $urlImagem = $this->imagemController->salvarUmaImagem($request);

             $req = array_merge($req, ['imagem' => $urlImagem]);
           }

          return $this->mostrarUm(Protocolo::create($req));

         });
     }

     private function validarModelo($request)
     {
       $request->validate([
         'autor' => 'exists:users,id',
         'nome' => 'required_without:autor|min:2|max:50',
         'imagem' => 'image',

         'latitude' =>
          array('required_with:longitude', 'regex:/^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,16}$/'),

         'longitude' =>
          array('required_with:latitude',
          'regex:/^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,16}$/'),
         'mensagem' => 'required|min:2|max:1000',

         'categoriaServico' => 'required|exists:categoria_servicos,id',
         'subCategoriaServico' => 'required|exists:sub_categoria_servicos,id',
       ]);
     }

     public function cadastrarProtocoloSemUsuario(Request $request)
     {
       if ($request->has('autor')) {
           $request->autor = $request->request->remove('autor');
       }

       $request->validate([
         'nome' => 'required|min:2|max:50',
         'imagem' => 'image',

         'latitude' =>
          array('required_with:longitude', 'regex:/^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,16}$/'),

         'longitude' =>
          array('required_with:latitude',
          'regex:/^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,16}$/'),

         'mensagem' => 'min:2|max:1000',

         'categoriaServico' => 'exists:categoria_servicos,id',
         'subCategoriaServico' => 'exists:sub_categoria_servicos,id',
       ]);

       $req = $request->all();

       $req = array_merge($req, ['categoriaServico' => $request->categoriaServico]);
       $req = array_merge($req, ['subCategoriaServico' => $request->subCategoriaServico]);
       $req = array_merge($req, ['status' => 1]);

       return DB::transaction(function() use($request, $req) {

         if($request->has('imagem')){

           $urlImagem = $this->imagemController->salvarUmaImagem($request);

           $req = array_merge($req, ['imagem' => $urlImagem]);
         }

        return $this->mostrarUm(Protocolo::create($req));

       });

     }



    /**
     * Retorna Protocolo por id.
     *
     * @param  \App\Protocolo  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $protocolos = Protocolo::findOrFail($id)->load(['status']);
        //$protocolos->autor == $request->user()->id ? $protocolos->usuario_proprietario = true : $protocolos->usuario_proprietario = false;

        if (auth('api')->user() ){
          if(($protocolos->autor != auth('api')->user()->id) && (auth('api')->user()->admin != \App\User::ADMIN)){
            $protocolos['replica'] = false;
          }else{
            $protocolos['replica'] = true;
          }
        }else{
          $protocolos['replica'] = false;
        }

        $protocolos->makeHidden(['autor', 'nome']);
        // if($protocolos->servico){
        //   $protocolos->servico->categoria = \App\CategoriaServico::where('id', $protocolos->servico->categoria)->get();
        //   $protocolos->servico->status = \App\Status::where('id', $protocolos->servico->status)->get();
        // }
        //
        // if($protocolos->ouvidoria){
        //   $protocolos->ouvidoria->categoria = \App\CategoriaOuvidoria::where('id', $protocolos->ouvidoria->categoria)->get();
        //   $protocolos->ouvidoria->status = \App\Status::where('id', $protocolos->ouvidoria->status)->get();
        // }
        return $protocolos;
    }

    public function mensagensPorProtocolo(Request $request, $idProtocolo)
    {
        $protocolos = Protocolo::findOrFail($idProtocolo)->load(['respostas']);
        foreach ($protocolos->respostas as $key => $value) {
          unset($value->autor, $value->id, $value->protocoloId);
        }

        return $protocolos->respostas;
    }

    /**
     * Atualiza Protocolo por id.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Protocolo  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request->validate([
        'autor' => 'exists:users,id',
        'nome' => 'min:2|max:50',
        'mensagem' => 'min:2|max:1000',
        'status' => 'exists:statuses,id',
      ]);

      if ($request->has('idAuxiliar')) {
          $request->idAuxiliar = $request->request->remove('idAuxiliar');
      }

      if ($request->has('protocolo')) {
          $request->idAuxiliar = $request->request->remove('protocolo');
      }

      Protocolo::where('id', $id)->update($request->all());

      return $this->mostrarUm(Protocolo::findOrFail($id));
    }

    /**
     * Remove Protocolo por id.
     *
     * @param  \App\Protocolo  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
      if($request->user()->isAdmin()){

        $modelo = \App\Protocolo::findOrFail($id);

        return DB::transaction(function() use($request, $modelo) {

          ($modelo->imagem) ? $this->imagemController->removerUmaImagem($modelo->imagem) : '';

          $modelo->delete();

          return $this->mostrarUm($modelo);

        });
      }

      return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);
    }


    public function meusProtocolos(Request $request)
    {
      return  \App\Protocolo::where('autor', $request->user()->id)->get()->load(['status']);
    }

    public function mensagensPorProtocoloCriar(Request $request, $idProtocolo)
    {
      $protocolos = Protocolo::findOrFail($idProtocolo);
      if(($protocolos->autor != $request->user()->id) && ($request->user()->admin != \App\User::ADMIN)) return $this->erroResponse('Não autorizado', 401);
      $request->request->add(['autor' => $request->user()->id]);
      $request->request->add(['protocoloId' => $idProtocolo]);
      $request->request->add(['interno' => $request->user()->admin == \App\User::ADMIN ? true : false]);

      $request->validate([
        'mensagem' => 'required|min:2|max:1000',
        'protocoloId' => 'required|exists:protocolos,id',
      ]);

      return ProtocoloResposta::create($request->all());
    }
}
