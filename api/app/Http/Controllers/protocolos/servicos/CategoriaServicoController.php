<?php

namespace App\Http\Controllers\protocolos\servicos;

use App\CategoriaServico;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\midias\ImagemController;

class CategoriaServicoController extends ApiController
{
  protected $imagemController;
  protected $nomeModelo;

  public function __construct(ImagemController $iC)
  {
    $this->imagemController = $iC;
    $this->nomeModelo = 'categoriaServicos';
    $this->middleware('auth:api')->except('index');
  }

    /**
     * Retorna lista de CategoriaServico.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return CategoriaServico::all()->load(['subCategoriaServicos', 'template']);
    }

    /**
     * Cria uma notícia.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
       if($request->user()->isAdmin()){
         $this->validarModelo($request);

         return DB::transaction(function() use($request) {

           $urlImagem = $this->imagemController->salvarUmaImagem($request);

           $novoArray = array_merge($request->all(), ['imagem' => $urlImagem]);

           return $this->mostrarUm(CategoriaServico::create($novoArray));
         });
       }
     }

     private function validarModelo($request)
     {
       $request->validate([
         'titulo' => 'required|min:2|max:50|unique:categoria_servicos',
         'descricao' => 'required|min:2|max:50',
         'imagem' => 'required|image',
         'template' => 'exists:templates,id',
       ]);
     }

    /**
     * Retorna CategoriaServico por id.
     *
     * @param  \App\CategoriaServico  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->mostrarUm(CategoriaServico::findOrFail($id)->load(['subCategoriaServicos', 'template']));
    }

    /**
     * Atualiza CategoriaServico por id.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CategoriaServico  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      if($request->user()->isAdmin()){

        $request->validate([
          'titulo' => 'min:2|max:50',
          'descricao' => 'min:2|max:50',
          'imagem' => 'image',
          'template' => 'exists:templates,id',
        ]);

        $modelo = CategoriaServico::findOrFail($id);

        return DB::transaction(function() use($request, $id, $modelo) {

          if($request->has('imagem')){

            if($modelo->imagem != null) $this->imagemController->removerUmaImagem($modelo->imagem);

            $urlImagem = $this->imagemController->salvarUmaImagem($request);

            $novoArray = array_merge($request->all(), ['imagem' => $urlImagem]);

            $modelo->update($novoArray);

            return $this->mostrarUm($modelo);

          }

          $modelo->update($request->all());

          return $this->mostrarUm($modelo);

        });
      }
    }

    /**
     * Remove CategoriaServico por id.
     *
     * @param  \App\CategoriaServico  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
      if($request->user()->isAdmin()){

        return DB::transaction(function() use($request, $id) {

          $modelo = \App\CategoriaServico::findOrFail($id);

          $modelo->delete();

          $this->imagemController->removerUmaImagem($modelo->imagem);

          return $this->mostrarUm($modelo);
        });
      }

    }
}
