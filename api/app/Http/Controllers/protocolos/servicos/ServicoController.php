<?php

namespace App\Http\Controllers\protocolos\servicos;

use App\Servico;
use App\Protocolo;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\midias\ImagemController;
use App\Http\Controllers\protocolos\ProtocoloController;

class ServicoController extends ApiController
{
  protected $imagemController;
  protected $nomeModelo;
  protected $protocoloController;

  public function __construct(ImagemController $iC, ProtocoloController $pC)
  {
    $this->imagemController = $iC;
    $this->protocoloController = $pC;
    $this->nomeModelo = 'servicos';
    $this->middleware('auth:api')->except('cadastrarSemUsuario');
  }

    /**
     * Retorna lista de Servico.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->mostrarTodos(Servico::all()->load(['categoria', 'status', 'protocolo']));
    }

    /**
     * Cria uma notícia.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
       if($request->user()){
          $request->request->add(['autor' => $request->user()->id]);
       }

         $this->validarModelo($request);

         return DB::transaction(function() use($request) {

           if($request->has('imagem')){
             $urlImagem = $this->imagemController->salvarUmaImagem($request);

             $novoArray = array_merge($request->all(), ['imagem' => $urlImagem]);

             $modeloSalvo = $this->protocoloController->store($request);

             $novoArray = array_merge($novoArray, ['protocolo' => $modeloSalvo->id]);

             $novoArray = array_merge($novoArray, ['status' => 1]);

             return $this->mostrarUm(Servico::create($novoArray)->load(['protocolo']));
           }

           $modeloSalvo = $this->protocoloController->store($request);

           $novoArray = array_merge($request->all(), ['protocolo' => $modeloSalvo->id]);

           $novoArray = array_merge($novoArray, ['status' => 1]);

           return $this->mostrarUm(Servico::create($novoArray)->load(['protocolo']));

         });
     }


     private function validarModelo($request)
     {
       $request->validate([
         'imagem' => 'image',
         'categoria' => 'required|exists:categoria_servicos,id',

         'latitude' =>
          array('required_with:longitude', 'regex:/^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,16}$/'),

         'longitude' =>
          array('required_with:latitude',
          'regex:/^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,16}$/'),
          'mensagem' => 'required|min:2|max:1000',
       ]);
     }

     public function cadastrarSemUsuario(Request $request)
     {
       if ($request->has('autor')) {
           $request->autor = $request->request->remove('autor');
       }

       $this->validarModelo($request);

       return DB::transaction(function() use($request) {

         if($request->has('imagem')){
           $urlImagem = $this->imagemController->salvarUmaImagem($request);

           $novoArray = array_merge($request->all(), ['imagem' => $urlImagem]);

           $modeloSalvo = $this->protocoloController->cadastrarProtocoloSemUsuario($request);

           $novoArray = array_merge($novoArray, ['protocolo' => $modeloSalvo->id]);

           $novoArray = array_merge($novoArray, ['status' => 1]);

           return $this->mostrarUm(Servico::create($novoArray)->load(['protocolo']));
         }

         $modeloSalvo = $this->protocoloController->cadastrarProtocoloSemUsuario($request);

         $novoArray = array_merge($request->all(), ['protocolo' => $modeloSalvo->id]);

         $novoArray = array_merge($novoArray, ['status' => 1]);

         return $this->mostrarUm(Servico::create($novoArray)->load(['protocolo']));


       });

     }

    /**
     * Retorna Servico por id.
     *
     * @param  \App\Servico  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->mostrarUm(Servico::findOrFail($id)->load(['categoria', 'status', 'protocolo']));
    }

    /**
     * Atualiza Servico por id.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Servico  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if($request->user()->isAdmin()){

        if ($request->has('autor')) $request->request->remove('autor');

        $request->validate([
          'imagem' => 'image',
          'categoria' => 'exists:categoria_servicos,id',
          'status' => 'exists:statuses,id',

          'latitude' =>
           array('required_with:longitude', 'regex:/^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,16}$/'),

          'longitude' =>
           array('required_with:latitude',
           'regex:/^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,16}$/'),

           'autor' => 'exists:users,id',
           'nome' => 'min:2|max:50',
           'mensagem' => 'min:2|max:1000',
        ]);

        $mensagem = $request->only('mensagem');

        return DB::transaction(function() use($request, $id, $mensagem) {

          $modelo = Servico::findOrFail($id);

          if($request->has('imagem')){
            if($modelo->imagem != null) $this->imagemController->removerUmaImagem($modelo->imagem);

            $urlImagem = $this->imagemController->salvarUmaImagem($request);

            $novoArray = array_merge($request->all(), ['imagem' => $urlImagem]);

            if($request->has('mensagem'))Protocolo::where('id', $modelo->protocolo)->first()->update(['mensagem' => $mensagem['mensagem']]);

            $modelo->update($novoArray);

            return $this->mostrarUm($modelo);

          }

          if($request->has('mensagem'))Protocolo::where('id', $modelo->protocolo)->first()->update(['mensagem' => $mensagem['mensagem']]);

          $modelo->update($request->only([
            'imagem',
            'categoria',
            'status',
            'latitude',
            'longitude',
            'mensagem',
          ]));

          return $this->mostrarUm($modelo);

        });
      }

    }

    /**
     * Remove Servico por id.
     *
     * @param  \App\Servico  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
      if($request->user()->isAdmin()){

        return DB::transaction(function() use($request, $id) {

          $modelo = \App\Servico::findOrFail($id);

          $modelo->delete();

          $modeloPai = $this->protocoloController->destroy($modelo->protocolo);

          $modelo->protocolo = [$modeloPai];

          $this->imagemController->removerUmaImagem($modelo->imagem);

          return $this->mostrarUm($modelo);

        });

      }
      return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);
    }

}
