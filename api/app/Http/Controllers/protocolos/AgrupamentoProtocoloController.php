<?php

namespace App\Http\Controllers\protocolos;

use App\AgrupamentoProtocolo;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\DB;

class AgrupamentoProtocoloController extends ApiController
{

  public function __construct()
  {
      $this->middleware('auth:api');

  }

  public function index(Request $request)
  {
    if($request->user()->isAdmin()){
      $modelos = AgrupamentoProtocolo::all()->load(['protocolos']);

      foreach ($modelos as $key => $value) {
        foreach ($value->protocolos as $key => $value) {
            $value->makeHidden(['agrupamento', 'id', 'created_at']);
        }
      }
      return $modelos;
    }
    return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);
  }


  public function store(Request $request)
  {
    if($request->user()->isAdmin()){

      $request->validate([
        'responsavel' => 'required|exists:users,id',
        'protocolos' => 'required|array',
        'protocolos.*.id' => 'required|exists:protocolos,id',
      ]);

      return DB::transaction(function() use($request) {

        if( ! \App\AgrupamentoProtocolo::where('responsavel', $request->responsavel)->exists()){

          $modelo = AgrupamentoProtocolo::create($request->all());

          foreach ($request->protocolos as $key => $value) {

            $ia = ['agrupamento' => $modelo->id, 'protocolo' => $value['id']];

            \App\IntermediarioAgrupamento::create($ia);
          }

          return $this->mostrarUm($modelo);
        }

        foreach ($request->protocolos as $key => $value) {

          $ia = ['agrupamento' => $request->responsavel, 'protocolo' => $value['id']];

          \App\IntermediarioAgrupamento::create($ia);
        }

        return $this->mostrarUm(AgrupamentoProtocolo::where('responsavel', $request->responsavel)->first());

      });

    }
    return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);
  }




  public function show(Request $request, $id)
  {

    if($request->user()->isAdmin()){
      $modelo = AgrupamentoProtocolo::findOrFail($id)->load(['protocolos']);

      foreach ($modelo->protocolos as $key => $value) {
          $value->makeHidden(['agrupamento', 'id', 'created_at']);
      }
      return $modelo;
    }
    return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);



  }

  public function update(Request $request, $id)
  {
    if($request->user()->isAdmin()){

      $request->validate([
        'responsavel' => 'required|exists:users,id',
      ]);

      $modelo = AgrupamentoProtocolo::findOrFail($id);

      $modelo->update($request->all());

      return $this->mostrarUm(AgrupamentoProtocolo::findOrFail($id));
    }
    return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);

  }

  public function destroy(Request $request, $id)
  {
    if($request->user()->isAdmin()){

      $modelo = AgrupamentoProtocolo::findOrFail($id);

      $modelo->delete();

      return $this->mostrarUm($modelo);
    }
    return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);

  }
}
