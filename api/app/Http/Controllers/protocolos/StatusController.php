<?php

namespace App\Http\Controllers\protocolos;

use App\Status;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\DB;

class StatusController extends ApiController
{
    /**
     * Retorna lista de Status.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Status::all();
    }

    /**
     * Cria uma notícia.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
         $this->validarModelo($request);

         return $this->mostrarUm(Status::create($request->all()));
     }

     private function validarModelo($request)
     {
       $request->validate([
         'titulo' => 'required|min:2|max:50|unique:statuses',
       ]);
     }

    /**
     * Retorna Status por id.
     *
     * @param  \App\Status  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->mostrarUm(Status::findOrFail($id));
    }

    /**
     * Atualiza Status por id.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Status  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request->validate([
        'titulo' => 'min:2|max:50|unique:statuses',
      ]);

      Status::where('id', $id)->update($request->all());

      return $this->mostrarUm(Status::findOrFail($id));
    }

    /**
     * Remove Status por id.
     *
     * @param  \App\Status  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $modelo = \App\Status::findOrFail($id);

      $modelo->delete();

      return $this->mostrarUm($modelo);
    }
}
