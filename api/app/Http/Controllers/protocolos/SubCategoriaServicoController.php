<?php

namespace App\Http\Controllers\protocolos;

use App\SubCategoriaServico;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class SubCategoriaServicoController extends ApiController
{
  public function __construct()
  {
      $this->middleware('auth:api');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index(Request $request)
     {
       if($request->user()->isAdmin()){
         return SubCategoriaServico::all();
       }
       return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);
     }

    public function store(Request $request)
    {
      if($request->user()->isAdmin()){

        $request->validate([
          'titulo' => 'required|min:2|max:128|unique:sub_categoria_servicos',
          'categoriaServico' => 'required|exists:categoria_servicos,id',
        ]);

        return $this->mostrarUm(SubCategoriaServico::create($request->all()));
      }
      return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SubCategoriaServico  $subCategoriaServico
     * @return \Illuminate\Http\Response
     */
     public function show(Request $request, $id)
     {
       if($request->user()->isAdmin()){
         return $this->mostrarUm(SubCategoriaServico::findOrFail($id));
       }
       return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);

     }

     public function update(Request $request, $id)
     {
       if($request->user()->isAdmin()){

         $request->validate([
           'titulo' => 'min:2|max:128|unique:sub_categoria_servicos',
           'categoriaServico' => 'exists:categoria_servicos,id',
         ]);

         $modelo = SubCategoriaServico::findOrFail($id);

         $modelo->update($request->all());

         return $this->mostrarUm(SubCategoriaServico::findOrFail($id));
       }
       return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);

     }

     public function destroy(Request $request, $id)
     {
       if($request->user()->isAdmin()){

         $modelo = SubCategoriaServico::findOrFail($id);

         $modelo->delete();

         return $this->mostrarUm($modelo);
       }
       return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);

     }
}
