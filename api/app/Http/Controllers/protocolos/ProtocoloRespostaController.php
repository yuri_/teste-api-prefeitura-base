<?php

namespace App\Http\Controllers\protocolos;

use App\ProtocoloResposta;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class ProtocoloRespostaController extends ApiController
{
    /**
     * Retorna lista de ProtocoloResposta.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->mostrarTodos(ProtocoloResposta::all()->load(['usuario']));
    }

    /**
     * Cria uma notícia.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
       $this->validarModelo($request);

       return $this->mostrarUm(ProtocoloResposta::create($request->all()));

     }

     private function validarModelo($request)
     {
       $request->validate([
         'autor' => 'required|exists:users,id',
         'protocoloId' => 'required|exists:protocolos,id',
         'interno' => 'required|boolean',
         'mensagem' => 'required|min:2|max:1000',
       ]);
     }

    /**
     * Retorna ProtocoloResposta por id.
     *
     * @param  \App\ProtocoloResposta  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->mostrarUm(ProtocoloResposta::findOrFail($id));
    }

    /**
     * Atualiza ProtocoloResposta por id.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProtocoloResposta  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request->validate([
        'autor' => 'exists:users,id',
        'protocoloId' => 'exists:protocolos,id',
        'interno' => 'boolean',
        'mensagem' => 'min:2|max:1000',
      ]);

      return ProtocoloResposta::where('id', $id)->update($request->all());
    }

    /**
     * Remove ProtocoloResposta por id.
     *
     * @param  \App\ProtocoloResposta  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $modelo = \App\ProtocoloResposta::findOrFail($id);

      $modelo->delete();

      return $this->mostrarUm($modelo);
    }
}
