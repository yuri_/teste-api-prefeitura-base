<?php

namespace App\Http\Controllers\informacoes;

use App\Informacao;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\midias\ImagemController;

class InformacaoController extends ApiController
{
    protected $imagemController;
    protected $nomeModelo;

    public function __construct(ImagemController $iC)
    {
      $this->imagemController = $iC;
      $this->nomeModelo = 'informacoes';
      $this->middleware('auth:api')->except('index', 'show');
    }

    /**
     * Retorna lista de Informacao.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->mostrarTodos(Informacao::all()->load(['categoria']));
    }

    /**
     * Cria uma notícia.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($request->user()->isAdmin()){

        $this->validarModelo($request);

        return DB::transaction(function() use($request) {

          if($request->has('imagem')){
            $urlImagem = $this->imagemController->salvarUmaImagem($request);

            $novoArray = array_merge($request->all(), ['imagem' => $urlImagem]);

            return $this->mostrarUm(Informacao::create($novoArray));
          }
          return $this->mostrarUm(Informacao::create($request->all()));
        });
      }else{
        return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);
      }

    }

    private function validarModelo($request)
    {
      $request->validate([
        'categoria' => 'required|exists:categoria_informacaos,id',
        'titulo' => 'required|min:2|max:150',
        'descricao' => 'required|min:2|max:100',
        'conteudo' => 'required|min:10|max:200000',
        'imagem' => 'image',
      ]);
    }

    /**
     * Retorna Informacao por id.
     *
     * @param  \App\Informacao  $Informacao
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->mostrarUm(Informacao::findOrFail($id));
    }

    /**
     * Atualiza Informacao por id.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Informacao  $Informacao
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if($request->user()->isAdmin()){

        $request->validate([
          'categoria' => 'exists:categoria_informacaos,id',
          'titulo' => 'min:2|max:150',
          'descricao' => 'min:2|max:100',
          'conteudo' => 'min:10|max:200000',
          'imagem' => 'image',
        ]);

        $modelo = Informacao::findOrFail($id);

        return DB::transaction(function() use($request, $modelo, $id) {

          if($request->has('imagem')){

            $urlImagem = $this->imagemController->salvarUmaImagem($request);

            $this->imagemController->removerUmaImagem($modelo->imagem);

            $novoArray = array_merge($request->all(), ['imagem' => $urlImagem]);

            $modelo->update($novoArray);

            return $this->mostrarUm(Informacao::findOrFail($id));
          }

          $modelo->update($request->all());
          return $this->mostrarUm(Informacao::findOrFail($id));

        });
      }else{
        return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);
      }

    }

    /**
     * Remove Informacao por id.
     *
     * @param  \App\Informacao  $Informacao
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $modelo = \App\Informacao::findOrFail($id);

      $modelo->delete();

      return $this->mostrarUm($modelo);
    }
}
