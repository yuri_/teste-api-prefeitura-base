<?php

namespace App\Http\Controllers\informacoes;

use App\CategoriaInformacao;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\DB;

class CategoriaInformacaoController extends ApiController
{

    public function __construct()
    {
        $this->middleware('auth:api')->except('index', 'show');
    }
    /**
     * Retorna lista de CategoriaInformacao.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return CategoriaInformacao::all()->load(['informacoes']);
    }

    /**
     * Cria uma notícia.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
       if($request->user()->isAdmin()){
         $this->validarModelo($request);

         return $this->mostrarUm(CategoriaInformacao::create($request->all()));
       }
       return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);

     }

     private function validarModelo($request)
     {
       $request->validate([
         'titulo' => 'required|min:2|max:25|unique:categoria_informacaos',
       ]);
     }

    /**
     * Retorna CategoriaInformacao por id.
     *
     * @param  \App\CategoriaInformacao  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        return $this->mostrarUm(CategoriaInformacao::findOrFail($id)->load(['informacoes']));
    }

    /**
     * Atualiza CategoriaInformacao por id.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CategoriaInformacao  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request->validate([
        'titulo' => 'required|min:2|max:25|unique:categoria_informacaos',
      ]);

      if($request->user()->isAdmin()){
        $modelo = CategoriaInformacao::findOrFail($id);

        $modelo->update($request->all());

        return $this->mostrarUm(CategoriaInformacao::findOrFail($id));
      }
    return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);

    }

    /**
     * Remove CategoriaInformacao por id.
     *
     * @param  \App\CategoriaInformacao  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
      if($request->user()->isAdmin()){

        $modelo = \App\CategoriaInformacao::findOrFail($id);

        $modelo->delete();

        return $this->mostrarUm($modelo);
      }
      return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);
    }

}
