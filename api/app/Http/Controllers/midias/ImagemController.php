<?php

namespace App\Http\Controllers\midias;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Validation\Rule;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class ImagemController extends ApiController
{
    /**
     * Retorna um Json com todoas as imagens do banco.
     *
     * @return \Json\Midia
     */
    public function index()
    {
      return $this->mostrarTodos(\App\Imagem::all());
    }

    /**
     * Cria uma nova imagem em banco.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Imagem
     */
    public function salvarUmaImagem(Request $request)
    {
      $this->validarUmaImagem($request);
      return $this->salvarImagem($request);
    }

    private function validarUmaImagem($request)
    {
      $request->validate([
          'imagem' => 'required|image',
      ]);

    }

    private function salvarImagem($request)
    {
        $identificador = uniqid(true);
        Storage::disk('localweb')->put($identificador . '.png', file_get_contents($request->file('imagem')), 'public');
        return env('STORAGE_URL_TO_RETURN', 'https://lss.locaweb.com.br/') . $identificador . '.png';
    }


    /**
     * Cria uma ou várias novas imagens em banco.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Imagem
     */
    public function store(Request $request, $nomeModelo)
    {
      $this->validarImagens($request);

      return $this->salvarImagens($request, $nomeModelo);
    }

    private function validarImagens($request)
    {
      $request->validate([
          'imagens' => 'required|array',
          'imagens.*.arquivo' => 'required|image',
          'imagens.*.titulo' => 'max:100',
          'imagens.*.descricao' => 'max:150',
          'imagens.*.tamanho' => Rule::in(['P', 'M', 'G']),
      ]);

    }

    private function salvarImagens($request, string $nomeModelo)
    {
      $imagensSalvas = Collection::make([]);

      foreach ($request->all()['imagens'] as $key => $value) {

        $caminhoImagemNoStorage =
        env('CAMINHO_ARMAZENAMENTO_IMAGENS').$nomeModelo.'/'.$request->modeloId;

        $nomeImagem = time().$key.$value['arquivo']->getClientOriginalName();

        $value['arquivo']->move($caminhoImagemNoStorage, $nomeImagem);

        $imagensSalvas->push(\App\Imagem::create(
          array_merge($value,
            ['caminho' => $caminhoImagemNoStorage.'/'.$nomeImagem, 'tamanho' => 'M'])
        ));
      }
      return $imagensSalvas;
    }

    /**
     * Busca uma imgem pelo seu Id.
     *
     * @param  int  $id
     * @return \Imagem
     */
    public function show($id)
    {
        return $this->mostrarUm(\App\Imagem::findOrFail($id));
    }

    /**
   * Faz o donwload de uma imagem, pelo seu Id.
   *
   * @param int $idArquivo
   * @return \Imagem
   */
    public function baixar($idArquivo)
    {
      $arquivo = \App\Imagem::findOrFail($idArquivo);
      return \Response::download($arquivo->caminho);
    }

    /**
     * Atualiza uma imagem no banco.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Imagem
     */
    public function update(Request $request, $id, string $nomeModelo)
    {

    }

    /**
     * Remove uma imagem pelo seu Id.
     *
     * @param  int  $id
     * @return \Imagem
     */
    public function destroy($id)
    {
        $imagem = \App\Imagem::findOrFail($id);
        $imagem->delete();
        unlink($imagem->caminho);

        return $imagem;

    }

    /**
     * Remove uma imagem pelo seu Id.
     *
     * @param  string  $caminho
     * @return \Imagem
     */
    public function removerUmaImagem($caminho)
    {
      $path = str_replace(env('STORAGE_URL_TO_RETURN'), "", $caminho);
      return Storage::disk('localweb')->delete($path);

    }

}
