<?php

namespace App\Http\Controllers\notificacoes;

use App\Notificacao;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class NotificacaoController extends ApiController
{
  public function __construct()
  {
    $this->middleware('auth:api');
  }
    /**
     * Retorna lista de Notificacao.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return ($request->user()->isAdmin()) ? $this->mostrarTodos(Notificacao::all()->load(['usuario'])) : response()->json(['error' => 'Não autorizado', 'code' => 401], 401);
    }

    /**
     * Cria uma Configuracão.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($request->user()->isAdmin()){

        $this->validarModelo($request);

        if($request->has('visualizada')) $request->request->remove('visualizada');
        $request->request->add(['visualizada' => 0]);

        $request->request->add(['usuario' => $request->user()->id]);

        return $this->mostrarUm(Notificacao::create($request->all()));
      }
      return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);
    }

    private function validarModelo($request)
    {
      $request->validate([
        'mensagem' => 'required|min:2|max:4000',
      ]);
    }

    public function update(Request $request, $id)
    {
      if($request->user()->isAdmin()){

        if ($request->has('usuario')) $request->request->remove('usuario');

        $request->validate([
          'mensagem' => 'required|min:2|max:4000',
          'visualizada' => 'required|boolean',
        ]);

        $modelo =  Notificacao::where('id', $id)->firstOrfail();
        $modelo->fill($request->all());
        $modelo->save();

        return $modelo;

      }
      return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);
    }

    /**
     * Retorna Notificacao por id.
     *
     * @param  \App\Notificacao  $Notificacao
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
      if($request->user()->isAdmin()){

        return Notificacao::where('id', $id)->firstOrFail()->load(['usuario']);

      }
      return response()->json(['error' => 'Não autorizado', 'code' => 401], 401);
    }
}
