<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\ApiResponser;
use App\Http\Controllers\ApiController;

class ApiController extends Controller
{
    use ApiResponser;

    public function __construct()
    {
      //$this->middleware('auth:api');
      //$this->middleware('client.credentials');
    }
}
