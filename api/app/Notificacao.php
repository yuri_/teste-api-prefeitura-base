<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Notificacao extends Model
{
  protected $fillable = [
    'mensagem',
    'visualizada',
    'usuario',
  ];

  protected $hidden = [
      'updated_at',
  ];

  public function usuario()
  {
      return $this->belongsTo('App\User', 'usuario');
  }

  /**
   * MUTADORES
   */

  public function getCreatedAtAttribute($value)
  {
    return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d/m/Y - H:i');
  }
}
