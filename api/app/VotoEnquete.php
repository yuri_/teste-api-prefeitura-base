<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class VotoEnquete extends Model
{
  protected $fillable = [
    'autor',
    'votoOpcao',
    'enquete',
  ];

  protected $hidden = [
      'updated_at', 'created_at'
  ];

  /**
   * RELACIONAMENTOS
   */

  /**
   * MUTADORES
   */

  public function getCreatedAtAttribute($value)
  {
    return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d/m/Y - H:i');
  }

}
