<?php

namespace App;
use Illuminate\Support\Carbon;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
  protected $fillable = [
      'nome',
      'cor',
  ];

  protected $hidden = [
      'updated_at',
  ];

  /**
   * MUTADORES
   */

  public function getCreatedAtAttribute($value)
  {
    return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d/m/Y H:i');
  }
}
