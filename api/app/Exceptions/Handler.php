<?php

namespace App\Exceptions;

use Exception;
use App\Http\ApiResponser;
use Illuminate\Database\QueryException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    use ApiResponser;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
      if($exception instanceof ValidationException)
        return $this->convertValidationExceptionToResponse($exception, $request);

      if ($exception instanceof ModelNotFoundException) {
          $modelName = strtolower(class_basename($exception->getModel()));

          return $this->
          erroResponse(
            "Não existe nenhum {$modelName} com esse identificador", 404);
      }

      if ($exception instanceof NotFoundHttpException) {
          return $this->erroResponse(
            'O endereço especificado não pôde ser achado', 404);
      }

      if ($exception instanceof MethodNotAllowedHttpException) {
          return $this->erroResponse(
            'Método não permitido!', 405);
      }

      if ($exception instanceof HttpException) {
          return $this->erroResponse($exception->getMessage(),
                                      $exception->getStatusCode());
      }

      if ($exception instanceof QueryException) {
          $erro = $exception->errorInfo;

          return $this->erroResponse($erro[2], 409);
      }

      if ($exception instanceof TokenMismatchException) {
          return redirect()->back()->withInput($request->input());
      }

      if ($exception instanceof AuthenticationException) {
          return $this->unauthenticated($request, $exception);
      }

      if ($exception instanceof AuthorizationException) {
          return $this->erroResponse($exception->getMessage(), 403);
      }

      if (config('app.debug')) {
          return parent::render($request, $exception);
      }

      return $this->erroResponse(
        'Erro inesperado. Tente novamente, ou entre em contato com o administrador do sistema', 500);

    }

    /**
     * Converte uma exceção de autorização em um response não autenticado.
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($this->isFrontend($request)) {
            return redirect()->guest('login');
        }

        return $this->erroResponse('Não autenticado.', 401);
    }

    private function isFrontend($request)
    {
        return $request->acceptsHtml() && collect($request->route()
                        ->middleware())->contains('web');
    }

    protected function convertValidationExceptionToResponse(
      ValidationException $e, $request)
    {
        $erros = $e->validator->errors()->getMessages();

        return $this->erroResponse($erros, 422);
    }
}
