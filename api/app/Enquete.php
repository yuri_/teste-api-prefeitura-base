<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Enquete extends Model
{
  protected $fillable = [
    'titulo',
    'descricao',
    'duracaoHoras',
    'resPrivado',
  ];

  protected $hidden = [
      'updated_at', 'duracaoHoras',
  ];

  /**
   * RELACIONAMENTOS
   */
   public function opcoes()
   {
       return $this->hasMany('App\VotoOpcao', 'enquete');
   }
  /**
   * MUTADORES
   */

  public function getCreatedAtAttribute($value)
  {
    return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d/m/Y - H:i');
  }

}
