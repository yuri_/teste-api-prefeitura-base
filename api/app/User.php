<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Carbon;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    const USU_VERIFIDADO = '1';
    const USU_NAO_VERIFICADO = '0';

    const ADMIN = 'true';
    const REGULAR = 'false';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'verificado', 'tokenVerificacao', 'fone', 'imagem'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'updated_at', 'id', 'tokenVerificacao',
    ];

    public function protocolos()
    {
        return $this->hasMany('App\Protocolo', 'autor');
    }

    public function linkedSocialAccounts()
    {
      return $this->hasMany(LinkedSocialAccount::class);
    }
    public function isAdmin()
    {
        return $this->admin == User::ADMIN;
    }

    /**
     * MUTADORES
     */

    public function getCreatedAtAttribute($value)
    {
      return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d/m/Y - H:i');
    }
}
