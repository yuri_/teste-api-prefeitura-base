<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class AgrupamentoProtocolo extends Model
{
    protected $fillable = [
        'responsavel',
    ];

    protected $hidden = [
        'updated_at',
    ];

    /**
     * MUTADORES
     */

    public function getCreatedAtAttribute($value)
    {
      return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d/m/Y H:i');
    }

    public function protocolos()
    {
      // return DB::table('intermediario_agrupamentos')
      //         ->join('protocolos', 'intermediario_agrupamentos.protocolo', '=', 'protocolos.id')
      //         ->select('protocolos.*')
      //         ->get();
      return $this->hasMany('App\IntermediarioAgrupamento', 'agrupamento');
    }
}
