<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Configuracoes extends Model
{
  protected $fillable = [
    'campo',
    'valor',
  ];

  protected $hidden = [
      'updated_at', 'id', 'created_at'
  ];
}
