<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class CategoriaNoticia extends Model
{
  protected $fillable = [
    'nome',
  ];

  protected $hidden = [
      'updated_at',
  ];

  /**
   * MUTADORES
   */

  public function getCreatedAtAttribute($value)
  {
    return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d/m/Y - H:i');
  }


}
