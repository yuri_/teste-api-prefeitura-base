<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Status extends Model
{
  const RECEBIDO = 'recebido';
  const EM_ANDAMENTO = 'em_andamento';
  const RESOLVIDO = 'resolvido';

  protected $fillable = [
      'titulo',
  ];

  protected $hidden = [
      'updated_at', 'created_at',
  ];

  public function getCreatedAtAttribute($value)
  {
    return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d/m/Y');
  }
}
