<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class CategoriaServico extends Model
{
  protected $fillable = [
    'titulo',
    'descricao',
    'imagem',
    'template',
  ];

  protected $hidden = [
      'updated_at',
  ];

  /**
   * RELACIONAMENTOS
   */

  public function subCategoriaServicos()
  {
      return $this->hasMany('App\SubCategoriaServico', 'categoriaServico');
  }
  public function template()
  {
      return $this->belongsTo('App\Template', 'template');
  }

  public function getCreatedAtAttribute($value)
  {
    return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d/m/Y - H:i');
  }
}
