<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProtocolosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('protocolos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('autor')->nullable()->unsigned();
            $table->string('nome')->nullable();
            $table->string('imagem', 400)->nullable();
            $table->double('latitude', 20, 14)->nullable();
            $table->double('longitude', 20, 14)->nullable();
            $table->longText('mensagem');

            $table->integer('categoriaServico')->unsigned();
            $table->integer('subCategoriaServico')->unsigned();
            $table->integer('status')->unsigned()->default(1);

            $table->timestamps();

            $table->foreign('autor')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('status')->references('id')->on('statuses')->onDelete('cascade');
            $table->foreign('categoriaServico')->references('id')->on('categoria_servicos');
            $table->foreign('subCategoriaServico')->references('id')->on('protocolos')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('protocolos');
    }
}
