<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformacaosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informacaos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo', 150);
            $table->string('descricao', 100);
            $table->string('imagem', 400)->nullable();
            $table->longText('conteudo');
            $table->integer('categoria')->unsigned()->index();
            $table->timestamps();

            $table->foreign('categoria')->references('id')
                  ->on('categoria_informacaos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('informacaos');
    }
}
