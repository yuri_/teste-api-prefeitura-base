<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('email')->unique();
            $table->bigInteger('fone')->unsigned()->nullable();
            $table->string('imagem', 400)->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('verificado')->default(\App\User::USU_NAO_VERIFICADO);
            $table->string('tokenVerificacao')->nullable();
            $table->string('admin')->default(\App\User::REGULAR);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
