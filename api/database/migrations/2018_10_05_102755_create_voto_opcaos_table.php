<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotoOpcaosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voto_opcaos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo', 40);
            $table->string('descricao', 128);
            $table->integer('enquete')->unsigned();
            $table->timestamps();

            $table->foreign('enquete')->references('id')->on('enquetes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voto_opcaos');
    }
}
