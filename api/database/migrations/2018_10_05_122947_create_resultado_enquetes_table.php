<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultadoEnquetesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resultado_enquetes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('enquete')->unsigned();
            $table->integer('votos')->unsigned();
            $table->integer('votoOpcao')->unsigned();
            $table->timestamps();

            $table->foreign('enquete')->references('id')->on('enquetes')->onDelete('cascade');
            $table->foreign('votoOpcao')->references('id')->on('voto_opcaos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resultado_enquetes');
    }
}
