<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriaServicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categoria_servicos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo', 50)->unique();
            $table->string('descricao', 50);
            $table->string('imagem', 400)->nullable();
            $table->integer('template')->unsigned()->nullable()->default(1);
            $table->timestamps();

            $table->foreign('template')->references('id')->on('templates');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categoria_servicos');
    }
}
