<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubCategoriaServicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_categoria_servicos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo', 128);
            $table->integer('categoriaServico')->unsigned()->index();
            $table->timestamps();

            $table->foreign('categoriaServico')->references('id')->on('categoria_servicos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_categoria_servicos');
    }
}
