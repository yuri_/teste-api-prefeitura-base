<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotoEnquetesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voto_enquetes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('enquete')->unsigned();
            $table->integer('autor')->unsigned();
            $table->integer('votoOpcao')->unsigned();
            $table->timestamps();

            $table->foreign('enquete')->references('id')->on('enquetes')->onDelete('cascade');
            $table->foreign('autor')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('votoOpcao')->references('id')->on('voto_opcaos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voto_enquetes');
    }
}
