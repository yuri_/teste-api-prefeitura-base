<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProtocoloRespostasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('protocolo_respostas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('autor')->unsigned();
            $table->longText('mensagem');
            $table->integer('protocoloId')->unsigned();
            $table->boolean('interno');
            $table->timestamps();

            $table->foreign('autor')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('protocoloId')->references('id')->on('protocolos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('protocolo_respostas');
    }
}
