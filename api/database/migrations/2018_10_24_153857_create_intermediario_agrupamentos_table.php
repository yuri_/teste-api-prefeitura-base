<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIntermediarioAgrupamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('intermediario_agrupamentos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agrupamento')->unsigned()->index();
            $table->integer('protocolo')->unsigned()->index();
            $table->timestamps();

            $table->foreign('agrupamento')->references('id')->on('agrupamento_protocolos');
            $table->foreign('protocolo')->references('id')->on('protocolos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('intermediario_agrupamentos');
    }
}
