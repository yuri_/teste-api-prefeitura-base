<?php

use Illuminate\Database\Seeder;

class VotoOpcao extends Seeder
{

    public function run()
    {
      $modelo = new \App\VotoOpcao();
      $modelo->titulo = "Muito satisfeito";
      $modelo->descricao = '';
      $modelo->enquete = 1;
      $modelo->save();

      $modelo = new \App\VotoOpcao();
      $modelo->titulo = "Satisfeito";
      $modelo->descricao = '';
      $modelo->enquete = 1;
      $modelo->save();

      $modelo = new \App\VotoOpcao();
      $modelo->titulo = "Regular";
      $modelo->descricao = '';
      $modelo->enquete = 1;
      $modelo->save();

      $modelo = new \App\VotoOpcao();
      $modelo->titulo = "Insatisfeito";
      $modelo->descricao = '';
      $modelo->enquete = 1;
      $modelo->save();

      $modelo = new \App\VotoOpcao();
      $modelo->titulo = "Muito insatisfeito";
      $modelo->descricao = '';
      $modelo->enquete = 1;
      $modelo->save();


      $modelo = new \App\VotoOpcao();
      $modelo->titulo = "Sim";
      $modelo->descricao = 'Estou de acordo';
      $modelo->enquete = 2;
      $modelo->save();

      $modelo = new \App\VotoOpcao();
      $modelo->titulo = "Sim";
      $modelo->descricao = 'Realmente necessita de investimentos.';
      $modelo->enquete = 2;
      $modelo->save();

      $modelo = new \App\VotoOpcao();
      $modelo->titulo = "Não";
      $modelo->descricao = 'Não acho necessário no momento.';
      $modelo->enquete = 2;
      $modelo->save();

      $modelo = new \App\VotoOpcao();
      $modelo->titulo = "Não";
      $modelo->descricao = 'Toma espaço dos comerciantes já existentes';
      $modelo->enquete = 2;
      $modelo->save();
    }
}
