<?php

use Illuminate\Database\Seeder;

class Enquete extends Seeder
{

    public function run()
    {
      $modelo = new \App\Enquete();
      $modelo->titulo = "Qual seu grau de satisfação com a atual gestão?";
      $modelo->descricao = 'A prefeitura vem por meio, desta enquete, consultar a opinião da comunidade a respeito da atual gestão.';
      $modelo->duracaoHoras = 36;
      $modelo->resPrivado = 0;
      $modelo->save();

      $modelo = new \App\Enquete();
      $modelo->titulo = "Ampliação dos investimentos na área de turismo?";
      $modelo->descricao = 'A prefeitura vem por meio, desta enquete, consultar a opinião da comunidade a respeito dos investimentos em turismo, especificamente na construção de novos quiosques.';
      $modelo->duracaoHoras = 12;
      $modelo->resPrivado = 1;
      $modelo->save();


    }
}
