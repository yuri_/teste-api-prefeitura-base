<?php

use Illuminate\Database\Seeder;

class ResultadoEnquete extends Seeder
{

    public function run()
    {
      $modelo = new \App\ResultadoEnquete();
      $modelo->enquete = 1;
      $modelo->votos = 0;
      $modelo->votoOpcao = 1;
      $modelo->save();

      $modelo = new \App\ResultadoEnquete();
      $modelo->enquete = 1;
      $modelo->votos = 0;
      $modelo->votoOpcao = 2;
      $modelo->save();

      $modelo = new \App\ResultadoEnquete();
      $modelo->enquete = 1;
      $modelo->votos = 0;
      $modelo->votoOpcao = 3;
      $modelo->save();

      $modelo = new \App\ResultadoEnquete();
      $modelo->enquete = 1;
      $modelo->votos = 0;
      $modelo->votoOpcao = 4;
      $modelo->save();

      $modelo = new \App\ResultadoEnquete();
      $modelo->enquete = 1;
      $modelo->votos = 0;
      $modelo->votoOpcao = 5;
      $modelo->save();

      $modelo = new \App\ResultadoEnquete();
      $modelo->enquete = 2;
      $modelo->votos = 0;
      $modelo->votoOpcao = 6;
      $modelo->save();

      $modelo = new \App\ResultadoEnquete();
      $modelo->enquete = 2;
      $modelo->votos = 0;
      $modelo->votoOpcao = 7;
      $modelo->save();

      $modelo = new \App\ResultadoEnquete();
      $modelo->enquete = 2;
      $modelo->votos = 0;
      $modelo->votoOpcao = 8;
      $modelo->save();

      $modelo = new \App\ResultadoEnquete();
      $modelo->enquete = 2;
      $modelo->votos = 0;
      $modelo->votoOpcao = 9;
      $modelo->save();
    }
}
