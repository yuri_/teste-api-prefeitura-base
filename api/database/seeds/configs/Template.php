<?php

use Illuminate\Database\Seeder;

class Template extends Seeder
{

    public function run()
    {
      $modelo = new \App\Template();
      $modelo->nome = "Básico";
      $modelo->save();

      $modelo = new \App\Template();
      $modelo->nome = "Completo";
      $modelo->cor = "primary";
      $modelo->save();

      $modelo = new \App\Template();
      $modelo->nome = "Alerta";
      $modelo->cor = "danger";
      $modelo->save();

    }
}
