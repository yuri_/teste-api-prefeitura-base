  <?php

use Illuminate\Database\Seeder;

class Protocolo extends Seeder
{

    public function run()
    {
      $modelo = new \App\Protocolo();
      $modelo->autor = 1;
      $modelo->imagem = 'https://igx.4sqi.net/img/general/600x600/25835854_IG80tKxyY1twcP0MhZQ_WYI-zxpIqSmksNTqPGCTCAU.jpg';
      $modelo->latitude = -5.8124020141;
      $modelo->longitude = -35.2047349513;
      $modelo->mensagem = 'Os postes da avenida Pedro Vasconcelos estão quebrados na localização selecionada.';
      $modelo->categoriaServico = 1;
      $modelo->subCategoriaServico = 1;
      $modelo->status = 2;
      $modelo->save();

      $modelo = new \App\Protocolo();
      $modelo->nome = 'José Francisco Pereira';
      $modelo->mensagem = 'Seria interessante melhorar contratar mais estagiários ou funcionarios, o atendimento na UPA está muito lento!';
      $modelo->latitude = -5.750;
      $modelo->longitude = -35.160;
      $modelo->categoriaServico = 1;
      $modelo->subCategoriaServico = 1;
      $modelo->status = 2;
      $modelo->save();

    }
}
