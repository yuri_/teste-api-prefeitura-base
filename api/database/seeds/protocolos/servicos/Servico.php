<?php

use Illuminate\Database\Seeder;

class Servico extends Seeder
{

    public function run()
    {
      $modelo = new \App\Servico();
      $modelo->imagem = "https://igx.4sqi.net/img/general/600x600/25835854_IG80tKxyY1twcP0MhZQ_WYI-zxpIqSmksNTqPGCTCAU.jpg";
      $modelo->categoria = 1;
      $modelo->status = 1;
      $modelo->protocolo = 1;
      $modelo->latitude = -5.8124020141;
      $modelo->longitude = -35.2047349513;
      $modelo->save();


    }
}
