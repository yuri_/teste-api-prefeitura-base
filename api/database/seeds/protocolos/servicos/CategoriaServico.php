<?php

use Illuminate\Database\Seeder;

class CategoriaServico extends Seeder
{

    public function run()
    {
      $modelo = new \App\CategoriaServico();
      $modelo->titulo = "Ouvidoria";
      $modelo->descricao = "Contato com a prefeitura";
      $modelo->imagem = "https://svgsilh.com/svg_v2/310399.svg";
      $modelo->template = 1;
      $modelo->save();

      $modelo = new \App\CategoriaServico();
      $modelo->titulo = "Iluminação Pública";
      $modelo->descricao = "Instalação e reparo";
      $modelo->imagem = "https://img.freepik.com/icones-gratis/poste-de-iluminacao_318-104296.jpg?size=338&ext=jpg";
      $modelo->template = 1;
      $modelo->save();

      $modelo = new \App\CategoriaServico();
      $modelo->titulo = "Sinalização de Trânsito";
      $modelo->descricao = "Semáforo Defeituoso";
      $modelo->imagem = "https://image.freepik.com/icones-gratis/semaforos_318-33094.jpg";
      $modelo->template = 1;
      $modelo->save();

      $modelo = new \App\CategoriaServico();
      $modelo->titulo = "Vegetação";
      $modelo->descricao = "Controle da vegetação";
      $modelo->imagem = "https://png.pngtree.com/element_origin_min_pic/00/02/07/6456823cea2c95d.jpg";
      $modelo->template = 1;
      $modelo->save();

      $modelo = new \App\CategoriaServico();
      $modelo->titulo = "Entulho";
      $modelo->descricao = "Remoção de entulho";
      $modelo->imagem = "https://comerciobrasilia.com.br/wp-content/uploads/2017/08/ENTULHO.png";
      $modelo->template = 1;
      $modelo->save();

    }
}
