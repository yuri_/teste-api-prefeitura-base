<?php

use Illuminate\Database\Seeder;

class SubCategoriaServico extends Seeder
{

    public function run()
    {
      $modelo = new \App\SubCategoriaServico();
      $modelo->titulo = "Poste quebrado";
      $modelo->categoriaServico = 2;
      $modelo->save();

      $modelo = new \App\SubCategoriaServico();
      $modelo->titulo = "Sinal falhando";
      $modelo->categoriaServico = 3;
      $modelo->save();

      $modelo = new \App\SubCategoriaServico();
      $modelo->titulo = "Árvore na pista";
      $modelo->categoriaServico = 4;
      $modelo->save();

      $modelo = new \App\SubCategoriaServico();
      $modelo->titulo = "Árvore em rede elétrica";
      $modelo->categoriaServico = 4;
      $modelo->save();

      $modelo = new \App\SubCategoriaServico();
      $modelo->titulo = "Sugestão";
      $modelo->categoriaServico = 1;
      $modelo->save();

      $modelo = new \App\SubCategoriaServico();
      $modelo->titulo = "Reclamação";
      $modelo->categoriaServico = 1;
      $modelo->save();

      $modelo = new \App\SubCategoriaServico();
      $modelo->titulo = "Elogio";
      $modelo->categoriaServico = 1;
      $modelo->save();
    }
}
