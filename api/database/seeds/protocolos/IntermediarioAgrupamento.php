<?php

use Illuminate\Database\Seeder;

class IntermediarioAgrupamento extends Seeder
{

  public function run()
  {
    $modelo = new \App\IntermediarioAgrupamento();
    $modelo->agrupamento = 1;
    $modelo->protocolo = 1;
    $modelo->save();

    $modelo = new \App\IntermediarioAgrupamento();
    $modelo->agrupamento = 2;
    $modelo->protocolo = 2;
    $modelo->save();


  }
}
