<?php

use Illuminate\Database\Seeder;

class ProtocoloResposta extends Seeder
{

    public function run()
    {
      $modelo = new \App\ProtocoloResposta();
      $modelo->autor = 2;
      $modelo->protocoloId = 1;
      $modelo->mensagem = 'Obrigado pela informação, seu protocolo foi anexado a uma ordem de serviço e encaminhado para ser efeturado o reparo';
      $modelo->interno = 1;
      $modelo->save();

      $modelo = new \App\ProtocoloResposta();
      $modelo->autor = 1;
      $modelo->protocoloId = 1;
      $modelo->mensagem = 'Foram reparados alguns postes da rua, porém ainda existe postes com lampadas queimadas!';
      $modelo->interno = 0;
      $modelo->save();

      $modelo = new \App\ProtocoloResposta();
      $modelo->autor = 2;
      $modelo->protocoloId = 1;
      $modelo->mensagem = 'Foi solicitado uma vistoria na região e solicitado o material para reparo dos postes que ainda apresentam problemas.';
      $modelo->interno = 1;
      $modelo->save();

      $modelo = new \App\ProtocoloResposta();
      $modelo->autor = 1;
      $modelo->protocoloId = 1;
      $modelo->mensagem = 'Tudo resolvido! Muito obrigado!.';
      $modelo->interno = 0;
      $modelo->save();

      $modelo = new \App\ProtocoloResposta();
      $modelo->autor = 2;
      $modelo->protocoloId = 2;
      $modelo->mensagem = 'Infelizmente na data do seu atendimento, ocorreu alguns impŕevistos com nossos funcionarios, o serviço já foi normalizado!';
      $modelo->interno = 1;
      $modelo->save();

      $modelo = new \App\ProtocoloResposta();
      $modelo->autor = 2;
      $modelo->protocoloId = 2;
      $modelo->mensagem = 'Visto que o protocolo foi aberto de forma anonima, não sendo possivel uma replica, marco como encerrado!';
      $modelo->interno = 1;
      $modelo->save();

    }
}
