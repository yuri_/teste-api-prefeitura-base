<?php

use Illuminate\Database\Seeder;

class Status extends Seeder
{

    public function run()
    {
      $modelo = new \App\Status();
      $modelo->titulo = "Recebido";
      $modelo->save();

      $modelo = new \App\Status();
      $modelo->titulo = "Encaminhado";
      $modelo->save();

      $modelo = new \App\Status();
      $modelo->titulo = "Resolvido";
      $modelo->save();

      $modelo = new \App\Status();
      $modelo->titulo = "Aguardando Materiais";
      $modelo->save();

      $modelo = new \App\Status();
      $modelo->titulo = "Encaminhado para resolução";
      $modelo->save();

      $modelo = new \App\Status();
      $modelo->titulo = "Aguardando resposta";
      $modelo->save();

      $modelo = new \App\Status();
      $modelo->titulo = "Recusada";
      $modelo->save();
    }
}
