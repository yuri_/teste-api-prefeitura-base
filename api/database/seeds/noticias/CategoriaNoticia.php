<?php

use Illuminate\Database\Seeder;

class CategoriaNoticia extends Seeder
{

    public function run()
    {
      $modelo = new \App\CategoriaNoticia();
      $modelo->nome = 'Mobilidade';
      $modelo->save();

      $modelo = new \App\CategoriaNoticia();
      $modelo->nome = 'Cultura e Lazer';
      $modelo->save();

      $modelo = new \App\CategoriaNoticia();
      $modelo->nome = 'Nota Oficial';
      $modelo->save();
    }
}
