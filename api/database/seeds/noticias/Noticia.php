<?php

use Illuminate\Database\Seeder;

class Noticia extends Seeder
{

    public function run()
    {

      $modelo = new \App\Noticia();
      $modelo->titulo = 'INAUGURADO NOVO SISTEMA DE INTEGRAÇÃO DE ÔNIBUS EM EXTREMOZ; VEJA O ITINERÁRIO';
      $modelo->imagem = 'http://extremoz.rn.gov.br/wp-content/uploads/2018/09/IMG-20180901-WA0061-1200x675.jpg';
      $modelo->categoria = 1;
      $modelo->conteudo = 'O Prefeito de Extremoz Joaz Oliveira inaugurou na
      manhã deste sábado (01),  o novo sistema integrado da linha 120A, Extremoz
       /Igapó que começa a operar na próxima segunda-feira (03) de setembro.
       solenidade contou com a presença de representantes da Empresa Oceano, motoristas
e obradores, vereadores Cleyton Saint Clair, Renato Leite, Jordão, Josias, Kilter,
Aderson e o Presidente da Câmara Fábio Vicente, além de secretários municipais, s
ervidores e usuários do sistema de transportes público.

A nova linha contará com cinco novos ônibus que atenderão os bairros como Moinho
dos Ventos, Central Parque Club, Centro, Jardins de Extremoz e adjacências até
igapó, em Natal. A frota realizará 40 viagens por dia com tarifa de R$ 3,25,

podendo o passageiro fazer integração com as linhas 120, 121 e 122.

Centenas de famílias serão beneficiadas com o novo sistema que ofertará uma
passagem mais barata para Natal com a possibilidade de realizar integração
para outros bairros da Capital.

Para o prefeito Joaz Oliveira, a implantação da nova linha irá melhorar a
qualidade de vida da população e permitirá o acesso de forma mais rápida aos
principais pontos de circulação de pessoas pelos bairros. “Essa nova linha
garante o atendimento à rotina urbana e facilitam a integração do município
com a Capital”, disse Joaz.';
      $modelo->save();

      $modelo = new \App\Noticia();
      $modelo->titulo = 'PREFEITURA DE EXTREMOZ REALIZOU O “EU SOU DA CULTURA”';
      $modelo->imagem = 'http://extremoz.rn.gov.br/wp-content/uploads/2018/09/capa-1200x675.jpg';
      $modelo->categoria = 2;
      $modelo->conteudo = 'A Prefeitura Municipal de Extremoz, através da Fundação
      de Cultural Aldeia Guajirú, realizou nos dias 30 e 31 de agosto, o evento
      “Eu Sou da Cultura”.
Aconteceram apresentações dos violeiros Chico Bento e seu Nenem; do Boi de Reis
de Zé Barrá, do Murici e de Dona Cecília, do Centro; do grupo de Carimbó da Escola
Franco Ribeiro, coordenado pela professora Lucélia; Aula oficina  de danças afro
com Professor Café; Manipulação e degustação do famoso grude com a grudeira Simone
Damasceno; Oficina de música com o Maestro da Banda Cláudio;Exposição de ervas
medicinais e degustação com a professora Andréia Barbosa, da Escola João Florêncio;
Oficina de karatê; Aula sobre a proteção do Meio Ambiente ministrada pelo professor
Nildo do CRAS; Apresentação do ator mirim Miguel Bruno que interpretou o poema
de sua autoria “Lampião da Terra do Grude” e a Exposição da História de Extremoz
 com Ricardo Barros.
O evento contou com a colaboração da Professora Maria da Paz que participou de
todas as atividades e de voluntários que contribuíram para a realização do evento
que foi um sucesso.
Para a Presidente da Fundação Aldeia Guajiú, Maria Gorette, o evento mobilizou
artistas do município que tiveram a oportunidade de realizar apresentações e
mostrar seus talentos e a população que conheceu os valores culturais, históricos
 e artísticos de Extremoz.';
      $modelo->save();

      $modelo = new \App\Noticia();
      $modelo->titulo = 'NOTA OFICIAL';
      $modelo->imagem = 'http://extremoz.rn.gov.br/wp-content/uploads/2018/08/Sem-t%C3%ADtulo22222.png';
      $modelo->categoria = 3;
      $modelo->conteudo = 'Prezados pais e/ou Responsáveis
Comunicamos a toda comunidade escolar, em especial aos pais e/ou Responsáveis que as crianças e jovens estão com acesso ilimitado ao fenômeno chamado MOMO, circulando nas redes sociais, principalmente pelo aplicativo WhatsApp.

Essa personagem tem aparência aterrorizante, olhos esbugalhados, pele pálida e um sorriso sinistro. Sua imagem ficou famosa pelo WhatsApp e disseminada como um desafio viral.

As crianças e jovens são estimuladas indevidamente a participarem de um jogo com desafios que vão do sufocamento até o enforcamento.

Diante desta realidade assustadora, orientamos e alertamos os pais e/ou Responsáveis a estarem cada vez mais próximos e atentos, conversem com seus filhos sobre o assunto para evitarem essa armadilha que continua tirando a paz das famílias e da sociedade.

Contamos com o apoio de todos para combater esse mal!

A Secretaria Municipal de Educação esta à disposição para maiores esclarecimentos.';
      $modelo->save();

    }
}
