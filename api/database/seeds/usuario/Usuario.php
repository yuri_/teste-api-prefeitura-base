<?php

use Illuminate\Database\Seeder;

class Usuario extends Seeder
{

    public function run()
    {
      $modelo = new \App\User();
      $modelo->name = 'Fernando Pereira';
      $modelo->email = 'usuario@01.com';
      $modelo->password = bcrypt('usuario01');
      $modelo->verificado = \App\User::USU_VERIFIDADO;
      $modelo->tokenVerificacao = str_random(40);
      $modelo->admin = \App\User::REGULAR;
      $modelo->save();

      $modelo = new \App\User();
      $modelo->name = 'Jose Francisco Neto';
      $modelo->email = 'usuario@02.com';
      $modelo->password = bcrypt('usuario02');
      $modelo->verificado = \App\User::USU_VERIFIDADO;
      $modelo->tokenVerificacao = str_random(40);
      $modelo->admin = \App\User::REGULAR;
      $modelo->save();

      $modelo = new \App\User();
      $modelo->name = 'Junior da Silva Mello';
      $modelo->email = 'usuario@03.com';
      $modelo->password = bcrypt('usuario0');
      $modelo->verificado = \App\User::USU_NAO_VERIFICADO;
      $modelo->tokenVerificacao = str_random(40);
      $modelo->admin = \App\User::REGULAR;
      $modelo->save();
    
      $modelo = new \App\User();
      $modelo->name = 'Gestor Admin';
      $modelo->email = 'gestor@admin.com';
      $modelo->password = bcrypt('gestor');
      $modelo->verificado = \App\User::USU_VERIFIDADO;
      $modelo->tokenVerificacao = str_random(40);
      $modelo->admin = \App\User::ADMIN;
      $modelo->save();
    }
}
