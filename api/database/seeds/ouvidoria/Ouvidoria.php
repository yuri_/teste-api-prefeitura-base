<?php

use Illuminate\Database\Seeder;

class Ouvidoria extends Seeder
{

    public function run()
    {
      $modelo = new \App\Ouvidoria();
      $modelo->categoria = 1;
      $modelo->status = 1;
      $modelo->protocolo = 2;
      $modelo->save();

    }
}
