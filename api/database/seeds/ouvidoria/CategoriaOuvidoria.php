<?php

use Illuminate\Database\Seeder;

class CategoriaOuvidoria extends Seeder
{

    public function run()
    {
      $modelo = new \App\CategoriaOuvidoria();
      $modelo->titulo = 'Sugestão';
      $modelo->save();

      $modelo = new \App\CategoriaOuvidoria();
      $modelo->titulo = 'Elogio';
      $modelo->save();

      $modelo = new \App\CategoriaOuvidoria();
      $modelo->titulo = 'Reclamação';
      $modelo->save();
    }
}
