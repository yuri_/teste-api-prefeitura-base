<?php

use Illuminate\Database\Seeder;

class CategoriaInformacao extends Seeder
{

    public function run()
    {
      $modelo = new \App\CategoriaInformacao();
      $modelo->titulo = 'Rede de saúde';
      $modelo->save();

      $modelo = new \App\CategoriaInformacao();
      $modelo->titulo = 'Pontos Turísticos';
      $modelo->save();

      $modelo = new \App\CategoriaInformacao();
      $modelo->titulo = 'Institucionais';
      $modelo->save();
    }
}
