<?php

use Illuminate\Database\Seeder;

class Informacao extends Seeder
{

    public function run()
    {
      $modelo = new \App\Informacao();
      $modelo->titulo = 'Hospital Regional';
      $modelo->categoria = 1;
      $modelo->descricao = 'Emergências e Cirurgias';
      $modelo->conteudo = '<strong>Endereço:</strong><br> R. Araquari, s/n - Potengi, Natal - RN<br><br>
  <strong>Contato:</strong><br> (84) 3223-4325<br><br>
  <strong>Horários de Atendimento:</strong><br>
  <strong>Segunda-feira:</strong> Atendimento 24 horas
  <br>
  <strong>Terça-feira:</strong> Atendimento 24 horas
  <br>
  <strong>Quarta-feira:</strong> Atendimento 24 horas
  <br>
  <strong>Quinta-feira:</strong> Atendimento 24 horas
  <br>
  <strong>Sexta-feira:</strong> Atendimento 24 horas
  <br>
  <strong>Sábado:</strong> Atendimento 24 horas
  <br>
  <strong>Domingo:</strong> Atendimento 24 horas
  <br>';
      $modelo->imagem = 'http://agorarn.com.br/files/uploads/2017/02/1-111-750x422.jpg';
      $modelo->save();

      $modelo = new \App\Informacao();
      $modelo->titulo = 'Lagoa de Pitangui';
      $modelo->categoria = 2;
      $modelo->descricao = 'Lagoa com acesso pelas dunas.';
      $modelo->conteudo = '<p>A cerca de 35km de Natal, no litoral norte do Estado, fica a Lagoa de Pitangui, que conta também com o Bar da Lagoa. É um lugar agradável e perfeito para descansar.&nbsp;Se estiver de carro, passar algumas horas na lagoa é um jeito interessante de se divertir. A Lagoa tem vários peixinhos pequenos e muitas mesas com cadeiras dentro da água. Os preços não são lá muito baixos, mas a variedade do cardápio é bem grande, não faltam opções na hora de pedir. Descansar por lá e aproveitar a lagoa é a melhor pedida.</p>';
      $modelo->imagem = 'https://www.praiasdenatal.com.br/wp-content/uploads/2014/08/lagoa-de-pitangui-dunas1.jpg';
      $modelo->save();
    }
}
