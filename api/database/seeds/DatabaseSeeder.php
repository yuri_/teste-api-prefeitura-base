<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        Artisan::call('migrate:refresh', ["--force"=> true ]);

        $this->call(Usuario::class);
        $this->call(CategoriaNoticia::class);
        $this->call(Noticia::class);

        $this->call(CategoriaInformacao::class);
        $this->call(Informacao::class);

        $this->call(Status::class);

        $this->call(Template::class);
        $this->call(CategoriaServico::class);
        $this->call(SubCategoriaServico::class);

        $this->call(AgrupamentoProtocolo::class);
        $this->call(Protocolo::class);
        $this->call(IntermediarioAgrupamento::class);
        $this->call(ProtocoloResposta::class);

        $this->call(Enquete::class);
        $this->call(VotoOpcao::class);
        $this->call(ResultadoEnquete::class);
    }
}
