#!/bin/bash

if [ $# != 1 ];then
 echo "ENTRADA INVALIDA MEU AMIGO!, OBRIGATORIO PASSAR O NOME DA PREFEITURA COMO PRIMEIRO ARGUMENTO DO COMANDO EU NÃO SOU VIDENTE!";
 exit
fi

if [ "$EUID" -ne 0 ]
  then echo "ME DE PODERES! PRECISO DE ROOT PARA SUBIR O SERVIDOR"
  exit
fi

filecompose="docker-compose/$1.yml"

if [[ (-f "$filecompose") ]]
then
	echo "(SUCESSO) $filecompose ENCONTRADO, INICIANDO SERVICO"
else
	echo "(ERRO) $filecompose NÃO ENCONTRADO VERIFIQUE SUA ENTRADA, NÃO POSSO SUBIR OQUE NÃO EXISTE."
    exit
fi

# Dá permissões à estas pastas pra o Servidor
echo "(MENSAGEM) SETANDO PERMISSÃO PARA OS ARQUIVOS NECESSARIOS..."
chmod 777 -R ./api/storage
chmod 777 -R ./api/bootstrap/cache
# Roda o docker-compose que sobe os containers
echo "(MENSAGEM) SUBINDO CONTAINERS..."
docker-compose -f $filecompose down 
docker-compose -f $filecompose build 
docker-compose -f $filecompose up
# Espera 5 segundos para que o servidor de banco possa inicializar

echo "(MENSAGEM) =) TUDO CERTO (= YUUUUPI (PS: Aguarda um pouquinho ta ? as migrates podem estar sendo executadas ainda)"