#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "ME DE PODERES! PRECISO DE ROOT PARA SUBIR O SERVIDOR"
  exit
fi

echo "(MENSAGEM) SETANDO PERMISSÃO PARA OS ARQUIVOS NECESSARIOS..."
chmod 777 -R ./api/storage
chmod 777 -R ./api/bootstrap/cache

for filecompose in docker-compose/*.yml; do 
    echo "(MENSAGEM) SUBINDO CONTAINERS PARA: $filecompose"
    docker-compose -f $filecompose down 
    docker-compose -f $filecompose build 
    docker-compose -f $filecompose up -d
    echo "(MENSAGEM) CONTAINER DO COMPOSE $filecompose EXECUTADOS"
done

