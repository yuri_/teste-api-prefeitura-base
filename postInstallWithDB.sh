#!/bin/bash

echo "(MENSAGEM) COMANDOS POST-INSTALL INICIANDO!"
 chmod 777 -R api/storage
 chmod 777 -R api/bootstrap/cache
 composer install --prefer-dist
 php artisan migrate
 php artisan db:seed
 php artisan passport:install
echo "(MENSAGEM) COMANDOS POST-INSTALL EXECUTADOS COM SUCESSO!"
 apache2-foreground
